<?php
/**
 * @file
 * Provide general access pages.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * View the status of testing.
 *
 * @return string HTML output.
 */
function pifr_server_page_status() {
  module_load_include('statistic.inc', 'pifr_server');

  drupal_add_css(drupal_get_path('module', 'pifr') . '/pifr.css');
  drupal_add_css(drupal_get_path('module', 'pifr_server') . '/pifr_server.css');

  $form = array();

  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General'),
    '#weight' => -10,
  );
  $form['general']['status'] = array(
    '#type' => 'markup',
    '#value' => t('<b>Status</b>: %status', array('%status' => (PIFR_ACTIVE ? 'enabled': 'disabled'))),
    '#weight' => -10,
  );

  // Generate a set of graphs for the tabset.
  $form['general']['graphs'] = array(
    '#type' => 'tabset',
    '#weight' => -9,
    '#prefix' => '<div id="pifr-graphs">',
    '#suffix' => '</div>',
  );
  $i = -10;
  foreach (array('queue', 'capacity', 'usage') as $graph) {
    $function = 'pifr_server_page_status_graph_' . $graph;
    $form['general']['graphs'][$graph] = array(
      '#type' => 'tabpage',
      '#title' => t(ucfirst($graph)),
      '#content' => $function(),
      '#weight' => $i++,
    );
  }

  // If queue is empty then default to the capacity graph.
  if (!$form['general']['graphs']['queue']['#content']) {
    $form['general']['graphs']['queue']['#content'] = '<em>' . t('Queue is empty.') . '</em>';
    $form['general']['graphs']['capacity']['#selected'] = TRUE;
  }

  $form['project_clients'] = array(
    '#type' => 'fieldset',
    '#title' => t('Project clients'),
    '#value' => pifr_server_page_status_client_list(PIFR_SERVER_CLIENT_TYPE_PROJECT),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight' => -8,
  );

  $form['test_clients'] = array(
    '#type' => 'fieldset',
    '#title' => t('Test clients'),
    '#value' => pifr_server_page_status_client_list(PIFR_SERVER_CLIENT_TYPE_TEST),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#weight' => -7,
  );

  drupal_alter('pifr_server_status_view', $form);

  return drupal_render($form);
}

/**
 * Generate a graph of the queue state.
 *
 * @return string HTML representing graph.
 */
function pifr_server_page_status_graph_queue() {
  // Initialize graph.
  $graph = array(
    '#chart_id' => 'pifr_queue',
    '#type' => CHART_TYPE_PIE_3D,
    '#size' => chart_size(400, 150),
    '#data' => array(),
    '#labels' => array(),
  );

  // Determine the number of tests that are active.
  $active_count = pifr_server_statistic_active_count();
  $graph['#data']['active'] = $active_count;
  $graph['#labels']['active'] = t('Active') . ': ' . $active_count;

  // Determine the number of queued tests.
  $queue_count = pifr_server_statistic_queue_count() - $active_count;
  $graph['#data']['queue'] = $queue_count;
  $graph['#labels']['queue'] = pifr_server_test_status(PIFR_SERVER_TEST_STATUS_QUEUED) . ': ' . $queue_count;

  if (!$graph['#data']['active'] && !$graph['#data']['queue']) {
    return FALSE;
  }

  return chart_render($graph);
}

/**
 * Generate a graph of the test network capacity.
 *
 * @return string HTML representing graph.
 */
function pifr_server_page_status_graph_capacity() {
  $enabled_count = pifr_server_statistic_enabled_count();
  $total_count = count(pifr_server_client_get_type(PIFR_SERVER_CLIENT_TYPE_TEST));

  $graph = array(
    '#chart_id' => 'pifr_capacity',
    '#title' => t('Test clients enabled: @enabled of @total.',
      array('@enabled' => $enabled_count, '@total' => $total_count)),
    '#type' => CHART_TYPE_GMETER,
    '#size' => chart_size(300, 150),
    '#data' => array($total_count ? $enabled_count / $total_count * 100 : 0),
  );
  return chart_render($graph);
}

/**
 * Generate a graph of the test network usage.
 *
 * @return string HTML representing graph.
 */
function pifr_server_page_status_graph_usage() {
  $active_count = pifr_server_statistic_active_count();
  $enabled_count = pifr_server_statistic_enabled_count();

  $graph = array(
    '#chart_id' => 'pifr_usage',
    '#title' => t('Test clients in use: @active of @enabled.',
      array('@active' => $active_count, '@enabled' => $enabled_count)),
    '#type' => CHART_TYPE_GMETER,
    '#size' => chart_size(300, 150),
    '#data' => array($enabled_count ? $active_count / $enabled_count * 100 : 0),
    '#data_colors' => array('00FF00', 'FFFF00', 'FF8040', 'FF0000'),
  );
  return chart_render($graph);
}

/**
 * Generate a list of clients.
 *
 * @param integer $client_type Client type, PIFR_SERVER_CLIENT_TYPE_*.
 * @return string HTML output.
 */
function pifr_server_page_status_client_list($client_type) {
  $clients = pifr_server_client_get_type($client_type);

  $header = array(t('Client'), t('Environment(s)'), t('Status'), t('Last active'), t('Manager'));
  $rows = array();
  foreach ($clients as $client) {
    if ($client['status'] == PIFR_SERVER_CLIENT_STATUS_DISABLED) {
      continue;
    }

    $row = array();
    $row[] = $client['client_id'];

    // Project clients do not related to an environment.
    if ($client['type'] == PIFR_SERVER_CLIENT_TYPE_TEST) {
      $row[] = pifr_server_client_environment_render($client);
    }

    // Determine the last time the client was active.
    $last_active = pifr_server_client_get_last_active($client);

    $link = array(
      'text' => pifr_server_client_status($client['status']),
      'href' => 'pifr/test/' . $client['test_id'],
    );

    if ($client['status'] == PIFR_SERVER_CLIENT_STATUS_ENABLED) {
      if ($last_active && $last_active['code'] == PIFR_SERVER_LOG_CLIENT_REQUEST) {
        $test = pifr_server_test_get($last_active['test_id']);
        $env = pifr_server_environment_status_get_client($test, $client);
        $link['text'] .= ' - Reviewing' . '<br />' . check_plain($test['title']) . '<br />' . 'Environment: ' . check_plain($env);
        $link['href'] = 'pifr/test/' . $last_active['test_id'];
      }
      else {
        $link['text'] .= ' - Idle';
      }
    }
    elseif ($client['status'] != PIFR_SERVER_CLIENT_STATUS_DISABLED) {
      // Status must be: PIFR_SERVER_CLIENT_STATUS_TESTING or
      // PIFR_SERVER_CLIENT_STATUS_FAILED so link to client test.
    }

    // Determine the amount of time the server has been idle.
    $idletime = $last_active ? time() - $last_active['timestamp'] : 0;

    $row[] = l($link['text'], $link['href'], array('html' => TRUE));
    $row[] = $last_active ? format_interval($idletime) . ' (' . pifr_server_format_date($last_active['timestamp']) . ')' : '--';
    $row[] = theme('username', user_load($client['uid']));

    // Determine the class to color the row with based on the idle time.
    if ($client['status'] == PIFR_SERVER_CLIENT_STATUS_ENABLED) {
      $class = 'pifr-pass';
    }
    elseif ($client['status'] == PIFR_SERVER_CLIENT_STATUS_TESTING) {
      $class = 'pifr-exception';
    }
    else {
      // Assumed to be failed testing.
      $class = 'pifr-fail';
    }

    $rows[] = array(
      'class' => $class,
      'data' => $row
    );
  }

  // Project clients do not related to an environment.
  if ($client_type == PIFR_SERVER_CLIENT_TYPE_PROJECT) {
    unset($header[1]);
  }

  // If rows then display table, otherwise empty message.
  if ($rows) {
    return theme('table', $header, $rows);
  }
  return '<em>' . t('No clients to display.') . '</em>';
}

/**
 * View statistics.
 *
 * @return string HTML output.
 */
function pifr_server_page_statistics() {
  module_load_include('statistic.inc', 'pifr_server');

  drupal_add_css(drupal_get_path('module', 'pifr_server') . '/pifr_server.css');

  $form = array();

  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General'),
    '#weight' => -10,
  );

  // Generate a table of general statistics.
  $general = pifr_server_statistic_general();
  $header = array(t('Key'), t('Value'));
  $rows = array(
    array(t('Number of tests reviewed'), number_format($general['test_count'])),
    array(t('Number of reviews performed'), number_format($general['review_count'])),
    array(t('Average number of times a test is reviewd'), number_format($general['review_average'])),
  );
  $form['general']['table'] = array(
    '#type' => 'markup',
    '#value' => theme('table', $header, $rows),
    '#weight' => -10,
  );

  // Genate result breakdown summary graph and one for each environment.
  $form['general']['results'] = array(
    '#type' => 'tabset',
    '#weight' => -9,
    '#prefix' => '<div id="pifr-graphs">',
    '#suffix' => '</div>',
  );
  $form['general']['results'][] = array(
    '#type' => 'tabpage',
    '#title' => t('Summary'),
    '#content' => pifr_server_page_statistics_breakdown_graph(pifr_server_statistic_breakdown()),
  );

  // Cycle through each environment and create a result breakdown graph.
  $environments = pifr_server_environment_get_all();
  foreach ($environments as $environment) {
    $breakdown = pifr_server_statistic_breakdown($environment['environment_id']);

    $form['general']['results'][$environment['environment_id']] = array(
      '#type' => 'tabpage',
      '#title' => check_plain($environment['title']),
      '#content' => pifr_server_page_statistics_breakdown_graph($breakdown, $environment['environment_id']),
    );
  }

  drupal_alter('pifr_server_statistics_view', $form);

  return drupal_render($form);
}

/**
 * Generate a breakdown graph.
 *
 * @param array $breakdown Breakdown information.
 * @param integer $environment_id (Optional) Environment ID.
 * @return string HTML output.
 */
function pifr_server_page_statistics_breakdown_graph(array $breakdown, $environment_id = 0) {
  // Initialize chart information.
  $chart = array(
    '#chart_id' => 'pifr_server_result_breakdown_' . $environment_id,
    '#title' => t('Result breakdown'),
    '#type' => CHART_TYPE_PIE_3D,
    '#size' => chart_size(600, 200),
    '#data' => array(),
    '#labels' => array(),
  );

  // If graphing an individual environment then graph data only needs to be
  // added for that environment, otherwise cycle through all the environments
  // combine the data.
  if ($environment_id) {
    pifr_server_page_statistics_breakdown_graph_data($chart['#data'], $chart['#labels'], $breakdown, $environment_id);
  }
  else {
    foreach ($breakdown as $environment_id => $data) {
      pifr_server_page_statistics_breakdown_graph_data($chart['#data'], $chart['#labels'], $data, $environment_id);
    }
  }

  // Sort data so that it appears cleanly in graph.
  asort($chart['#data']);

  return chart_render($chart);
}

/**
 * Generate breakdown graph data and labels.
 *
 * @param array $data Reference to graph data.
 * @param array $labels Reference to graph labels.
 * @param array $breakdown Breakdown information.
 * @param integer $environment_id (Optional) Environment ID.
 */
function pifr_server_page_statistics_breakdown_graph_data(array &$data, array &$labels, array $breakdown, $environment_id) {
  foreach ($breakdown as $code => $count) {
    // Use a key to ensure that data and labels match up after being sorted.
    $key = $environment_id . ':' . $code;

    // Add data and label representing code to chart.
    $data[$key] = $count;
    $labels[$key] = pifr_server_review_code_title($environment_id, $code);
  }
}
