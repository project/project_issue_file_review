<?php
/**
 * @file
 * Provides documentation of API hooks.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Called when a result is received from a test client.
 *
 * @param array $test Test information.
 * @param array $result Most result information.
 */
function hook_pifr_server_result($test, $result) {
  // Ensure that testing is complete.
  if ($test['status'] == PIFR_SERVER_TEST_STATUS_RESULT) {
    // Do something.
  }
}

/**
 * Called when a client test result is received from a test client.
 *
 * @param array $test Test information.
 * @param array $result Most result information.
 * @param array $client Client infomration associated with test.
 */
function hook_pifr_server_client_result($test, $result, $client) {
  // Ensure that testing is complete.
  if ($test['status'] == PIFR_SERVER_TEST_STATUS_RESULT) {
    // Do something.
  }
}
