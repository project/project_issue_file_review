<?php

/**
 * @file
 * Provide project functions.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Load a project.
 *
 * @param integer $project_id Project ID.
 * @return array Project information, or FALSE.
 */
function pifr_server_project_get($project_id) {
  $result = db_query('SELECT *
                      FROM {pifr_project}
                      WHERE project_id = %d', $project_id);
  return db_fetch_array($result);
}

/**
 * Load all projects associated with a client.
 *
 * @param integer $client_id Client ID.
 * @return array List of projects.
 */
function pifr_server_project_get_client($client_id) {
  $result = db_query('SELECT *
                      FROM {pifr_project}
                      WHERE client_id = %d', $client_id);
  $projects = array();
  while ($project = db_fetch_array($result)) {
    $projects[] = $project;
  }
  return $projects;
}

/**
 * Find a project if it exists.
 *
 * @param array $project Project information.
 * @return array Complete project information if found, otherwise FALSE.
 */
function pifr_server_project_find(array $project) {
  $result = db_query("SELECT *
                      FROM {pifr_project}
                      WHERE client_identifier = '%s'
                      AND client_id = %d", $project['client_identifier'], $project['client_id']);
  return db_fetch_array($result);
}

/**
 * Save project information.
 *
 * @param array $project Project information.
 * @param bolean $check Check for existing project and update if found.
 * @return array Updated project information.
 */
function pifr_server_project_save(array $project, $check = TRUE) {
  if ($check && empty($project['project_id'])) {
    // Check if project already exists.
    if ($project_record = pifr_server_project_find($project)) {
      $project['project_id'] = $project_record['project_id'];
    }
  }

  if (!empty($project['project_id'])) {
    // Existing project, update record.
    drupal_write_record('pifr_project', $project, 'project_id');
  }
  else {
    // New project, insert record.
    drupal_write_record('pifr_project', $project);
  }
  return $project;
}

/**
 * Delete a project and all branches associated with it.
 *
 * @param array $project Project information.
 */
function pifr_server_project_delete(array $project) {
  // Delete all branches that are associated with the project.
  $branches = pifr_server_branch_get_all($project['project_id']);
  foreach ($branches as $branch) {
    pifr_server_branch_delete($branch);
  }

  // Delete project record.
  db_query('DELETE FROM {pifr_project} WHERE project_id = %d', $project['project_id']);
}
