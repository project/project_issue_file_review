<?php
/**
 * Field handler to present client type as string.
 */
class views_handler_field_pifr_client_type extends views_handler_field {
  function render($values) {
    return pifr_server_client_type($values->{$this->field_alias});
  }
}
