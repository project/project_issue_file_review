<?php
/**
 * Field handler to present client environment as string.
 */
class views_handler_field_pifr_client_environment extends views_handler_field {
  function render($values) {
    return pifr_server_client_environment($values->{$this->field_alias});
  }
}
