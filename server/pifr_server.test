<?php

/**
 * @file
 * Provide test of functionality.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Base test class for testing PIFR server functionality.
 */
class PIFRServerTestCase extends DrupalWebTestCase {

  /**
   * Administrative user.
   *
   * @var object
   */
  protected $admin;

  /**
   * Associative array of environments keyed by plugin module name.
   *
   * @var array
   */
  protected $environments = array();

  /**
   * Associative array of clients keyed by client type.
   *
   * @var array
   */
  protected $clients = array();

  protected function setUp() {
    parent::setUp('chart', 'pifr', 'tabs', 'views', 'pifr_server', 'pifr_assertion', 'pifr_drupal', 'pifr_coder', 'pifr_simpletest');

    // Create user that has the necessary PIFR permissions.
    $this->admin = $this->drupalCreateUser(array(
      'add pifr project client',
      'add pifr test client',
      'manage pifr environments',
      'manage pifr clients',
      'manage pifr tests',
      'administer pifr',
      'view all pifr tests',
      'access administration pages',
    ));
    $this->drupalLogin($this->admin);

    // Setup basic environment with two clients.
    $environment_ids = array();
    foreach (array('pifr_coder', 'pifr_simpletest') as $environment) {
      $this->environments[$environment] = $this->createEnvironment($environment);
      $environment_ids[] = $this->environments[$environment]['environment_id'];
    }
    $this->clients = array(
      'project' => $this->createClient(PIFR_SERVER_CLIENT_TYPE_PROJECT),
      'test' => $this->createClient(PIFR_SERVER_CLIENT_TYPE_TEST, $environment_ids),
    );

    // Configure settings based on environment.
    $edit = array(
      'pifr_debug' => TRUE,
      'pifr_server_client_test_interval' => -1,
    );
    $this->drupalPost('admin/pifr/configuration', $edit, t('Save configuration'));
  }

  /**
   * Create a client through the web interface.
   *
   * @param integer Type of client to create, PIFR_SERVER_CLIENT_TYPE_*.
   * @param array List of environment IDs of which the client can review.
   * @return array Client information upon success, otherwise FALSE.
   */
  protected function createClient($type, array $environment_ids = array()) {
    $uid = $this->loggedInUser->uid;

    // Visit the add client page.
    $this->drupalGet("user/$uid/pifr/add");

    // Retrieve the client key from the hidden input.
    $input = $this->xpath('.//input[@name="client_key"]');
    $client_key = (string) $input[0]['value'];
    $this->assertTrue($client_key, t('Client key found.'));

    // Fill in client form.
    $edit = array(
      'url' => url('', array('absolute' => TRUE)),
      'type' => $type,
    );

    // Fill in the checkboxes for each environment ID.
    foreach ($environment_ids as $environment_id) {
      $edit['environment[' . $environment_id . ']'] = TRUE;
    }

    // Save the client.
    $this->drupalPost(NULL, $edit, t('Save'));
    $this->assertText(t('Changes saved successfully.'));

    // Get client ID.
    $client_id = db_result(db_query("SELECT client_id FROM {pifr_client} WHERE client_key = '%s'", $client_key));
    if ($this->assertTrue($client_id, 'Client ID found')) {
      // Ensure that the client has the specified values.
      $this->drupalGet("user/$uid/pifr/edit/$client_id");
      $this->assertFieldByName('client_key', $client_key);
      foreach ($edit as $key => $value) {
        $this->assertFieldByName($key, $value);
      }

      return pifr_server_client_get($client_id);
    }
    return FALSE;
  }

  /**
   * Create an environment through the web interface.
   *
   * @param string $plugin Plugin module.
   * @param array $arguments Associative array of plugin arguments.
   * @return array Environment information upon success, otherwise FALSE.
   */
  protected function createEnvironment($plugin, array $arguments = array()) {
    $edit = array(
      'title' => $this->randomName(),
      'description' => $this->randomString(64),
      'plugin' => $plugin,
      'plugin_argument' => '',
    );
    foreach ($arguments as $key => $value) {
      $edit['plugin_argument'] .= "$key:$value\n";
    }
    $this->drupalPost('admin/pifr/environment/add', $edit, t('Save'));
    $this->assertText(t('Environment saved successfully.'));

    $environment_id = db_result(db_query('SELECT environment_id FROM {pifr_environment} WHERE title = "%s"', $edit['title']));
    if ($this->assertTrue($environment_id, 'Environment ID found')) {
      // Ensure that the environment has the specified values.
      $this->drupalGet('admin/pifr/environment/edit/' . $environment_id);
      foreach ($edit as $key => $value) {
        $this->assertFieldByName($key, $value);
      }

      return pifr_server_environment_get($environment_id);
    }
    return FALSE;
  }

  /**
   * Extract the text contained by the element.
   *
   * @param $element
   *   Element to extract text from.
   * @return
   *   Extracted text.
   */
  protected function asText(SimpleXMLElement $element) {
    if (!is_object($element)) {
      return $this->fail('The element is not an element.');
    }
    return trim(html_entity_decode(strip_tags($element->asXML())));
  }
}

/**
 * Confirm the various client management interfaces function properly.
 */
class PIFRServerManagementTestCase extends PIFRServerTestCase {

  public static function getInfo() {
    return array(
      'name' => 'Management',
      'description' => 'Confirm the various client management interfaces function properly.',
      'group' => 'PIFR',
    );
  }

  /**
   * Confirm the various client management interfaces function properly.
   */
  protected function testManagement() {
    // Attempt to set invalid values.
    $edit = array();
    foreach ($this->environments as $environment) {
      $edit['environment[' . $environment['environment_id'] . ']'] = FALSE;
    }
    $this->drupalPost('admin/pifr/server/edit/' . $this->clients['test']['client_id'], $edit, t('Save'));
    $this->assertText(t('At least one environment must be selected.'));

    $edit = array(
      'owner' => '17',
    );
    $this->drupalPost(NULL, $edit, t('Save'));
    $this->assertText(t('Invalid username.'));

    // Request project client to be enabled.
    $this->drupalPost('admin/pifr/server/enable/' . $this->clients['project']['client_id'], array(), t('Start testing'));

    // Ensure that project client is marked as enabled.
    $this->drupalGet('pifr/test/' . $this->clients['project']['test_id']);
    $this->assertText(t('Enabled'));

    // Request test client to be enabled.
    $this->drupalPost('admin/pifr/server/enable/' . $this->clients['test']['client_id'], array(), t('Start testing'));

    // Ensure that project client is marked as enabled.
    $this->drupalGet('pifr/test/' . $this->clients['test']['test_id']);
    $this->assertText(t('Testing client'));

    // Ensure that toggle buttons work correctly.
    $this->drupalGet('admin/pifr');
    $this->assertText(t('State') . ": \n " .t('disabled'));
    $this->assertText(t('Report') . ": \n " .t('disabled'));

    $this->drupalPost(NULL, array(), t('Toggle state'));
    $this->assertText(t('State') . ": \n " .t('enabled'));
    $this->assertText(t('Report') . ": \n " .t('disabled'));

    $this->drupalPost(NULL, array(), t('Toggle report'));
    $this->assertText(t('State') . ": \n " .t('enabled'));
    $this->assertText(t('Report') . ": \n " .t('enabled'));
  }
}

/**
 * Confirm that the various permission work as expected.
 */
class PIFRServerAccessTestCase extends PIFRServerTestCase {

  public static function getInfo() {
    return array(
      'name' => 'Access',
      'description' => 'Confirm that the various permission work as expected.',
      'group' => 'PIFR',
    );
  }

  /**
   * Confirm that the various permission work as expected.
   */
  protected function testAccess() {
    $this->clients['project2'] = $this->createClient(PIFR_SERVER_CLIENT_TYPE_PROJECT);

    // Enable project clients.
    $this->clients['project']['status'] = PIFR_SERVER_CLIENT_STATUS_ENABLED;
    $this->clients['project2']['status'] = PIFR_SERVER_CLIENT_STATUS_ENABLED;
    pifr_server_client_save($this->clients['project']);
    pifr_server_client_save($this->clients['project2']);

    // Activate pifr_server.
    variable_set('pifr_active', TRUE);

    // Queue up basic tests.
    $test_id = 4;
    foreach (array('project', 'project2') as $key) {
      $this->drupalPost('admin/pifr', array('client_id' => $this->clients[$key]['client_id']), t('Queue tests'));
      $this->assertText('Queued test: #' . $test_id++);
      $this->assertText('Queued test: #' . $test_id++);
      $this->assertText('Queued test: #' . $test_id++);
    }

    // Current user should be able to view all 9 tests.
    $this->assertAccess(array(1, 2, 3, 4, 5, 6, 7, 8, 9));

    // Only the first 3 tests are client confirmation tests.
    $this->drupalLogin($this->drupalCreateUser(array('view all pifr client tests')));
    $this->assertAccess(array(1, 2, 3));

    // The middle 3 tests (and project client) are from project client 1.
    $this->drupalLogin($user = $this->drupalCreateUser());
    $this->setOwner('project', $user);
    $this->assertAccess(array(1, 4, 5, 6));

    // The last 3 tests (and project client) are from project client 1.
    $this->drupalLogin($user = $this->drupalCreateUser());
    $this->setOwner('project2', $user);
    $this->assertAccess(array(3, 7, 8, 9));

    // The last 6 tests (and project client) are from project clients 1 and 2.
    $this->setOwner('project', $user);
    $this->assertAccess(array(1, 3, 4, 5, 6, 7, 8, 9));

    // Anonymous users do not have access to anything.
    $this->drupalLogout();
    $this->assertAccess();

    // Anonymous users can view all tests related to public client.
    $this->setPublic('project');
    $this->assertAccess(array(1, 4, 5, 6));

    $this->setPublic('project2');
    $this->assertAccess(array(1, 3, 4, 5, 6, 7, 8, 9));
  }

  /**
   * Assert that a user has access to a set of tests.
   *
   * @param array $access List of test IDs the user should have access to.
   */
  protected function assertAccess(array $access = array()) {
    $paths = array(
      'pifr/log/',
      'pifr/log/recent/',
      'pifr/log/client/',
      'pifr/log/client/recent/',
    );

    // Cycle through all tests and ensure that the proper access is enforced.
    for ($i = 1; $i <= 9; $i++) {
      $this->drupalGet('pifr/test/' . $i);
      $this->assertAccessText($i, $access);


      $this->drupalGet('pifr/log/' . $i);
      $this->assertAccessText($i, $access);

      $this->drupalGet('pifr/log/recent/' . $i);
      $this->assertAccessText($i, $access);

      // Since the clients created in the test should have the same ID as their
      // corresponding confirmation test we can ensure that both logs have the
      // proper access controls.
      if ($i <= 3) {
        $this->drupalGet('pifr/log/client/' . $i);
        $this->assertAccessText($i, $access);

        $this->drupalGet('pifr/log/client/recent/' . $i);
        $this->assertAccessText($i, $access);
      }
    }
  }

  /**
   * Assert that the page demonstrates the proper access.
   *
   * @param integer $i ID that should have proper access.
   * @param array $access List of IDs that should have access.
   */
  protected function assertAccessText($i, array $access) {
    if (in_array($i, $access)) {
      $this->assertNoText(t('Access denied'));
    }
    else {
      $this->assertText(t('Access denied'));
    }
  }

  /**
   * Set client owner to a user.
   *
   * @param string $key Key used to reference client in test.
   * @param object $user User object.
   */
  protected function setOwner($key, $user) {
    $this->clients[$key]['uid'] = $user->uid;
    pifr_server_client_save($this->clients[$key]);
  }

  /**
   * Set client public field to TRUE.
   *
   * @param string $key Key used to reference client in test.
   */
  protected function setPublic($key) {
    $this->clients[$key]['public'] = TRUE;
    pifr_server_client_save($this->clients[$key]);
  }
}

/**
 * Run through the various XML-RPC calls and ensure that they function properly.
 */
class PIFRServerXMLRPCTestCase extends PIFRServerTestCase {

  /**
   * Batch of test information.
   *
   * @var array
   */
  protected $batch = array(
    'branches' => array(
      array(
        'project_identifier' => '3',
        'client_identifier' => '4',
        'vcs_identifier' => 'git',
        'dependency' => '2,6',
        'plugin_argument' => array(
          'modules' => array(
            'foo',
            'foofoo',
          ),
        ),
        'test' => FALSE,
        'link' => 'http://example.com/node/4',
      ),
      array(
        'project_identifier' => '1',
        'client_identifier' => '2',
        'vcs_identifier' => 'master',  // GITMIGRATION fragile
        'dependency' => '',
        'plugin_argument' => array(
          'core' => '7',
        ),
        'test' => FALSE,
        'link' => 'http://example.com/node/2',
      ),
      array(
        'project_identifier' => '5',
        'client_identifier' => '6',
        'vcs_identifier' => 'master',   // GITMIGRATION fragile
        'dependency' => '2,4',
        'plugin_argument' => array(
          'modules' => array(
            'bar',
            'barbar',
          ),
        ),
        'test' => FALSE,
        'link' => 'http://example.com/node/6',
      ),
    ),
    'files' => array(
      array(
        'branch_identifier' => '4',
        'client_identifier' => '1',
        'file_url' => 'http://example.com/sites/pift.d6x.loc/files/simpletest283193/issues/pass.patch',
        'link' => 'http://example.com/node/7',
      ),
      array(
        'branch_identifier' => '4',
        'client_identifier' => '2',
        'file_url' => 'http://example.com/sites/pift.d6x.loc/files/simpletest283193/issues/pass_0.patch',
        'link' => 'http://example.com/node/7#comment-1',
      ),
      array(
        'branch_identifier' => '4',
        'client_identifier' => '3',
        'file_url' => 'http://example.com/sites/pift.d6x.loc/files/simpletest283193/issues/pass_1.patch',
        'link' => 'http://example.com/node/7#comment-2',
      ),
      array(
        'branch_identifier' => '4',
        'client_identifier' => '4',
        'file_url' => 'http://example.com/sites/pift.d6x.loc/files/simpletest283193/issues/pass_2.patch',
        'link' => 'http://example.com/node/7#comment-3',
      ),
    ),
    'projects' => array(
      array(
        'client_identifier' => '3',
        'name' => 'Foo',
        'repository_type' => 'git',
        'repository_url' => 'git://git.drupal.org/project/foo.git',
        'link' => 'http://example.com/node/3',
      ),
      array(
        'client_identifier' => '1',
        'name' => 'Drupal',
        'repository_type' => 'git',
        'repository_url' => 'git://git.drupal.org/project/drupal.git',
        'name' => 'Bar',
        'repository_type' => 'git',
        'repository_url' => 'git://git.drupal.org/project/bar.git',
        'link' => 'http://example.com/node/5',
      ),
    ),
  );

  public static function getInfo() {
    return array(
      'name' => 'XML-RPC',
      'description' => 'Run through the various XML-RPC calls and ensure that they function properly.',
      'group' => 'PIFR',
    );
  }

  /**
   * Run through the various XML-RPC calls and ensure that they function properly.
   */
  protected function testAPI() {
    // Enable test and project clients.
    $this->clients['project']['status'] = PIFR_SERVER_CLIENT_STATUS_ENABLED;
    $this->clients['test']['status'] = PIFR_SERVER_CLIENT_STATUS_ENABLED;
    pifr_server_client_save($this->clients['project']);
    pifr_server_client_save($this->clients['test']);

    // Activate pifr_server.
    variable_set('pifr_active', TRUE);
    variable_set('pifr_server_report', TRUE);
    variable_set('pifr_server_client_test_interval', -1);

    // Queue batch of test information.
    $time = time() - 1;
    $url = url('', array('absolute' => TRUE)) . 'xmlrpc.php';
    $response = xmlrpc($url, 'pifr.queue', $this->clients['project']['client_key'], $this->batch);

    // Ensure that all test IDs are unique.
    $test_ids = array();
    foreach (array('branches', 'files') as $type) {
      foreach ($response[$type] as $key => $test_id) {
        $this->assertFalse(in_array($test_id, $test_ids), 'Unique test id [' . $test_id . ']');
        $test_ids[] = $test_id;
      }
    }

    // Ensure that test IDs are still returned when tests are double queued.
    $response = xmlrpc($url, 'pifr.queue', $this->clients['project']['client_key'], $this->batch);
    $found = 0;
    foreach (array('branches', 'files') as $type) {
      foreach ($response[$type] as $key => $test_id) {
        if (in_array($test_id, $test_ids)) {
          $found++;
        }
      }
    }
    $this->assertEqual($found, count($test_ids), 'All test IDs were returned even when tests were still queued');

    // Ensure that the XML-RPC information is correct.
    $test = pifr_server_test_get($response['branches'][4]);
    $environments = pifr_server_environment_test_get_all($test['test_id']);
    foreach ($environments as $environment) {
      $export = pifr_server_test_xmlrpc($test, $environment);
      $this->assertEqual($export['test_id'], $test['test_id'], 'Test ID matches');
      $this->assertEqual($export['review']['plugin'], $environment['plugin'], 'Plugin matches');
      $this->assertEqual($export['review']['argument']['core'], 7, 'Core compatibility matches');
      $this->assertEqual($export['review']['argument']['modules'], array('foo', 'foofoo'), 'Modules list matches');
      $this->assertEqual($export['vcs']['main']['repository']['url'], $this->batch['projects'][0]['repository_url'], 'Main repository URL matches');
      $this->assertEqual($export['vcs']['dependencies'][0]['repository']['url'], $this->batch['projects'][1]['repository_url'], 'Dependency[0] repository URL matches');
      $this->assertEqual($export['vcs']['dependencies'][1]['repository']['url'], $this->batch['projects'][2]['repository_url'], 'Dependency[1] repository URL matches');
      $this->assertEqual($export['files'], array(), 'Files list matches');
    }

    // Ensure that branch information is correct.
    $branch = pifr_server_branch_get_test($test['test_id']);
    $this->assertEqual($branch['branch_id'], 1, 'Branch ID matches');
    $this->assertEqual($branch['project_id'], 1, 'Project ID matches');
    $this->assertEqual($branch['test_id'], $test['test_id'], 'Test ID matches');
    $this->assertEqual($branch['client_identifier'], 4, 'Client identifier matches');
    $this->assertEqual($branch['vcs_identifier'], 'master', 'VCS identifier matches'); // GITMIGRATION fragile
    $this->assertEqual($branch['plugin_argument']['modules'], array('foo', 'foofoo'), 'Modules list matches');
    $this->assertEqual($branch['link'], 'http://example.com/node/4', 'Link matches');

    // Request tests and send results for each test until there are no more.
    $count = 0;
    while (TRUE) {
      $response = xmlrpc($url, 'pifr.next', $this->clients['test']['client_key']);
      if (!empty($response['test_id'])) {
        $result = array(
          'test_id' => $response['test_id'],
          'code' => 1,
          'details' => array(),
          'data' => array(),
          'log' => '',
        );
        $response = xmlrpc($url, 'pifr.result', $this->clients['test']['client_key'], $result);
        $this->assertEqual($response, PIFR_RESPONSE_ACCEPTED, 'Test result [' . $result['test_id'] . '] was accepted');
        $count++;
      }
      else {
        break;
      }
    }

    // Three branches are registered, but not queued (2 environments).
    $this->assertEqual($count, (count($test_ids) - 3) * 2, 'All tests reviewed');

    module_load_include('xmlrpc.inc', 'pifr_server');
    $time = pifr_server_xmlrpc_time_gmt($time);

    // Retieve results since test started and ensure proper result.
    $response = xmlrpc($url, 'pifr.retrieve', $this->clients['project']['client_key'], $time);
    $this->assertEqual(count($response['results']), count($test_ids) - 3, 'Correct results returned');
    $this->assertFalse($response['next'], 'No more results');
    $this->assertTrue($response['last'] >= $time, 'Last timestamp is valid');

    // Ensure that no tests are returned using 'last' timestamp.
    $last = (int) $response['last'];
    $response = xmlrpc($url, 'pifr.retrieve', $this->clients['project']['client_key'], $last);
    $this->assertFalse($response['results'], 'No results returned after new timestamp');
    $this->assertFalse($response['next'], 'No more results');
    $this->assertEqual($response['last'], $last, 'Last timestamp is valid');
  }

  /**
   * Ensure that the pifr.queue() validation routines work properly.
   */
  protected function testValidation() {
    $url = url('', array('absolute' => TRUE)) . 'xmlrpc.php';
    $batch = array();

    // Ensure that 'bogus' client key is rejected.
    $response = xmlrpc($url, 'pifr.queue', 'bogus', $batch);
    $this->assertError($response, PIFR_RESPONSE_INVALID_SERVER, 'Invalid project client, check key and ensure that client is enabled.');

    // Ensure that disabled project client is rejected.
    $response = xmlrpc($url, 'pifr.queue', $this->clients['project']['client_key'], $batch);
    $this->assertError($response, PIFR_RESPONSE_INVALID_SERVER, 'Invalid project client, check key and ensure that client is enabled.');

    // Ensure that valid project client is deneied when server is not active.
    $this->clients['project']['status'] = PIFR_SERVER_CLIENT_STATUS_ENABLED;
    pifr_server_client_save($this->clients['project']);
    $response = xmlrpc($url, 'pifr.queue', $this->clients['project']['client_key'], $batch);
    $this->assertError($response, PIFR_RESPONSE_DENIED, 'Server is not active.');

    // Enable server and ensure that request is not denied.
    variable_set('pifr_active', TRUE);
    $response = xmlrpc($url, 'pifr.queue', $this->clients['project']['client_key'], $batch);
    $this->assertEqual($response['branches'], array(), 'No branches queued');
    $this->assertEqual($response['files'], array(), 'No branches queued');

    // Begin validation of various parts of the queue batch.
    $batch = array(
      'branches' => array(),
      'files' => array(),
      'projects' => array(),
      'rawr' => array(),
    );
    $response = xmlrpc($url, 'pifr.queue', $this->clients['project']['client_key'], $batch);
    $this->assertError($response, PIFR_RESPONSE_DENIED, 'Only keys [branches, files, projects] are allowed.');

    // Check the various project validation.
    unset($batch['rawr']);
    $batch['projects'][] = array(
      'name' => 'Example',
      'repository_type' => 'rawr',
      'repository_url' => 'http://example.com/',
    );
    $batch['projects'][] = array(
      'client_identifier' => '17a',
      'name' => 'Example 2',
      'repository_type' => 'cvs',
      'repository_url' => 'http://example.com/2',
      'rawr' => 'hi',
    );
    $response = xmlrpc($url, 'pifr.queue', $this->clients['project']['client_key'], $batch);
    $this->assertError($response, PIFR_RESPONSE_DENIED, array(
      'Error(s) found in project #1: required key [client_identifier] not found.',
      'Error(s) found in project #2: Only keys [client_identifier, name, repository_type, repository_url, link] are allowed.',
    ));

    unset($batch['projects'][1]['rawr']);
    $response = xmlrpc($url, 'pifr.queue', $this->clients['project']['client_key'], $batch);
    $this->assertError($response, PIFR_RESPONSE_DENIED, array(
      'Error(s) found in project #1: required key [client_identifier] not found.',
      'Error(s) found in project #2: [client_identifier] must be an integer.',
    ));

    $batch['projects'][0]['client_identifier'] = 17;
    $batch['projects'][1]['client_identifier'] = 17;
    $response = xmlrpc($url, 'pifr.queue', $this->clients['project']['client_key'], $batch);
    $this->assertError($response, PIFR_RESPONSE_DENIED, array(
      'Error(s) found in project #1: [repository_type] must be one of [cvs, bzr].',
      'Error(s) found in project #2: [client_identifier] must be unique.',
    ));

    // Check the various branch validation.
    $batch['projects'][0]['repository_type'] = 'cvs';
    $batch['projects'][1]['client_identifier'] = 18;
    $batch['branches'][] = array(
      'project_identifier' => '17a',
      'client_identifier' => '170a',
      'vcs_identifier' => 'HEAD',
    );
    $batch['branches'][] = array(
      'project_identifier' => 18,
      'client_identifier' => 180,
      'vcs_identifier' => 'HEAD',
      'dependency' => '170',
      'rawr' => 'hi',
      'plugin_argument' => 'foo',
    );
    $response = xmlrpc($url, 'pifr.queue', $this->clients['project']['client_key'], $batch);
    $this->assertError($response, PIFR_RESPONSE_DENIED, array(
      'Error(s) found in branch #1: [project_identifier] must be an integer, [client_identifier] must be an integer.',
      'Error(s) found in branch #2: Only keys [project_identifier, client_identifier, vcs_identifier, dependency, plugin_argument, test, link] are allowed.',
    ));

    unset($batch['branches'][1]['rawr']);
    $response = xmlrpc($url, 'pifr.queue', $this->clients['project']['client_key'], $batch);
    $this->assertError($response, PIFR_RESPONSE_DENIED, array(
      'Error(s) found in branch #1: [project_identifier] must be an integer, [client_identifier] must be an integer.',
      'Error(s) found in branch #2: invalid dependency [170], [plugin_argument] must be an array.',
    ));

    // Check the various file validation.
    $batch['branches'][0]['project_identifier'] = 17;
    $batch['branches'][0]['client_identifier'] = 170;
    $batch['branches'][1]['plugin_argument'] = array('foo' => 'bar');
    $batch['files'][] = array(
      'branch_identifier' => 171,
      'client_identifier' => '1700a',
      'file_url' => 'http://example.com/foo.patch',
      'rawr' => 'hi',
    );
    $response = xmlrpc($url, 'pifr.queue', $this->clients['project']['client_key'], $batch);
    $this->assertError($response, PIFR_RESPONSE_DENIED, array(
      'Error(s) found in file #1: Only keys [branch_identifier, client_identifier, file_url, link] are allowed.',
    ));

    unset($batch['files'][0]['rawr']);
    $response = xmlrpc($url, 'pifr.queue', $this->clients['project']['client_key'], $batch);
    $this->assertError($response, PIFR_RESPONSE_DENIED, array(
      'Error(s) found in file #1: [branch_identifier] not found in branch list, [client_identifier] must be an integer.',
    ));

    $batch['files'][0]['branch_identifier'] = 170;
    $batch['files'][0]['client_identifier'] = 1700;
    $response = xmlrpc($url, 'pifr.queue', $this->clients['project']['client_key'], $batch);
    $this->assertEqual($response['branches'], array(
      170 => 3,
      180 => 4,
    ), 'Branches saved');
    $this->assertEqual($response['files'], array(
      1700 => 5,
    ), 'File queued');
  }

  /**
   * Assert that an error was returned in the XML-RPC response.
   *
   * @param array $response XML-RPC response.
   * @param integer $code XML-RPC response code to assert.
   * @param mixed $error Either an array or errors to expect or a single error.
   */
  protected function assertError($response, $code, $error) {
    $this->assertEqual($code, $response['response'], 'Response code equal to [' . $code . ']');
    if (is_array($error)) {
      $this->assertEqual($error, $response['errors'], 'Response errors found');
    }
    else {
      debug($response['errors']);
      $this->assertEqual($error, $response['errors'][0], 'Response error found [' . $error . ']');
    }
  }
}
