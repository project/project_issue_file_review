<?php

/**
 * @file
 * Provide environment management and convenience functions.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Environment form.
 */
function pifr_server_environment_form(&$form_state, $environment_id = NULL) {
  if ($environment_id) {
    $environment = pifr_server_environment_get($environment_id);
    foreach (array('client', 'project', 'branch') as $key) {
      $environment[$key] = pifr_server_environment_format($key, $environment[$key]);
    }
  }
  else {
    $environment = array(
      'title' => '',
      'description' => '',
      'client' => '',
      'project' => '',
      'branch' => '',
      'type' => '',
      'plugin' => '',
      'plugin_argument' => array(),
    );
  }

  $form = array();

  $form['environment_id'] = array(
    '#type' => 'value',
    '#value' => $environment_id,
  );

  $form['environment'] = array(
    '#type' => 'fieldset',
    '#title' => t('Environment'),
  );
  $form['environment']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('Title of environment.'),
    '#default_value' => $environment['title'],
    '#required' => TRUE,
  );
  $form['environment']['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#description' => t('Description of the environment.'),
    '#default_value' => $environment['description'],
  );

  $form['environment']['condition'] = array(
    '#type' => 'fieldset',
    '#title' => t('Conditions'),
    '#description' => t('If you leave a condition blank it will be considered a wildcard. ' .
                        'Separate multiple values with a comma which will be treated as an OR condition.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['environment']['condition']['client'] = array(
    '#type' => 'textfield',
    '#title' => t('Client ID(s)'),
    '#description' => t('The client ID(s) to restrict environment to.'),
    '#default_value' => $environment['client'],
    '#autocomplete_path' => 'admin/pifr/environment/ahah/client',
  );
  $form['environment']['condition']['project'] = array(
    '#type' => 'textfield',
    '#title' => t('Project(s)'),
    '#description' => t('The project(s) to restrict environment to.'),
    '#default_value' => $environment['project'],
    '#autocomplete_path' => 'admin/pifr/environment/ahah/project',
  );
  $form['environment']['condition']['branch'] = array(
    '#type' => 'textfield',
    '#title' => t('Branch(s)'),
    '#description' => t('The branch(es) to restrict environment to.'),
    '#default_value' => $environment['branch'],
    '#autocomplete_path' => 'admin/pifr/environment/ahah/branch',
  );
  $form['environment']['condition']['type'] = array(
    '#type' => 'select',
    '#title' => t('Type(s)'),
    '#description' => t('The type(s) of test(s) to restrict environment to.'),
    '#default_value' => $environment['type'],
    '#multiple' => TRUE,
    '#options' => array(
      PIFR_SERVER_TEST_TYPE_CLIENT,
      PIFR_SERVER_TEST_TYPE_BRANCH,
      PIFR_SERVER_TEST_TYPE_FILE,
    ),
  );
  $form['environment']['condition']['type']['#options'] = drupal_map_assoc($form['environment']['condition']['type']['#options'], 'pifr_server_test_type');

  $form['environment']['review'] = array(
    '#type' => 'fieldset',
    '#title' => t('Review'),
    '#description' => t('Configure the review to take place.'),
    '#collapsible' => TRUE,
    '#collapsed' => (bool) $environment_id,
  );
  $form['environment']['review']['plugin'] = array(
    '#type' => 'select',
    '#title' => t('Plugin'),
    '#description' => t('The type of review.'),
    '#options' => array(0 => t('--')) + pifr_server_review_get_all(),
    '#default_value' => $environment['plugin'],
    '#required' => TRUE,
  );
  $form['environment']['review']['plugin_argument'] = array(
    '#type' => 'textarea',
    '#title' => t('Argument(s)'),
    '#description' => t('Each argument should be placed on a separate line following the format [KEY]:[VALUE]. Will be overriden by any branch specific argument(s).'),
    '#default_value' => '',
  );
  foreach ($environment['plugin_argument'] as $key => $value) {
    // If the value is an array then print each item on a separate line and
    // append '[]' to the key so it will be parsed as an array.
    if (is_array($value)) {
      $key .= '[]';
      foreach ($value as $item) {
        $form['environment']['review']['plugin_argument']['#default_value'] .= "$key:$item\n";
      }
    }
    else {
      $form['environment']['review']['plugin_argument']['#default_value'] .= "$key:$value\n";
    }
  }

  $form['op'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['cancel'] = array(
    '#type' => 'markup',
    '#value' => l(t('Cancel'), 'admin/pifr/environment'),
  );

  return $form;
}

/**
 * Validate environment.
 */
function pifr_server_environment_form_validate($form, &$form_state) {
  // Ensure that conditions are valid.
  if (pifr_server_environment_lookup('client', $form_state['values']['client']) === FALSE) {
    form_set_error('client', t('Invalid client ID(s).'));
  }
  if (pifr_server_environment_lookup('project', $form_state['values']['project']) === FALSE) {
    form_set_error('project', t('Invalid project(s).'));
  }
  if (pifr_server_environment_lookup('branch', $form_state['values']['branch']) === FALSE) {
    form_set_error('branch', t('Invalid branch(s).'));
  }

  // Ensure a review plugin has been choosen.
  if (!$form_state['values']['plugin']) {
    form_set_error('plugin', t('Must choose a review plugin.'));
  }
}

/**
 * Save environment.
 */
function pifr_server_environment_form_submit($form, &$form_state) {
  foreach (array('client', 'project', 'branch') as $key) {
    $form_state['values'][$key] = pifr_server_environment_lookup($key, $form_state['values'][$key]);
  }

  $lines = explode("\n", $form_state['values']['plugin_argument']);
  $form_state['values']['plugin_argument'] = array();
  foreach ($lines as $line) {
    if ($line = trim($line)) {
      $parts = explode(':', $line, 2);
      if (!empty($parts[0]) && !empty($parts[1])) {
        // If the key ends with '[]' then treat it as an array.
        if (substr($parts[0], -2) == '[]') {
          $form_state['values']['plugin_argument'][substr($parts[0], 0, -2)][] = $parts[1];
        }
        else {
          $form_state['values']['plugin_argument'][$parts[0]] = $parts[1];
        }
      }
    }
  }

  pifr_server_environment_save($form_state['values']);
  drupal_set_message('Environment saved successfully.');

  $form_state['redirect'] = 'admin/pifr/environment';
}

/**
 * Provide suggestions for environment conditions via AHAH.
 *
 * @param string $type Type of suggestions being requested.
 * @param string $query Query string to find suggestions for.
 */
function pifr_server_environment_ahah($type, $query) {
  if (!in_array($type, array('client', 'project', 'branch'))) {
    return;
  }

  $parts = array_map('trim', explode(',', $query));
  $query = array_pop($parts);
  $prefix = implode(', ', $parts);
  $prefix = $prefix ? $prefix . ', ' : '';

  if ($type == 'client') {
    $result = db_query('SELECT client_id
                        FROM {pifr_client}
                        WHERE client_id LIKE %d
                        ORDER BY client_id
                        LIMIT 10', $query);
  }
  elseif ($type == 'project') {
    $result = db_query("SELECT name
                        FROM {pifr_project}
                        WHERE name LIKE '%%%s%%'
                        ORDER BY name
                        LIMIT 10", $query);
  }
  elseif ($type == 'branch') {
    $result = db_query("SELECT client_identifier
                        FROM {pifr_branch}
                        WHERE client_identifier LIKE '%%%s%%'
                        ORDER BY client_identifier
                        LIMIT 10", $query);
  }

  $suggestions = array();
  while ($suggestion = db_result($result)) {
    $suggestions[$prefix . $suggestion] = filter_xss($suggestion);
  }
  drupal_json($suggestions);
}

/**
 * Lookup IDs from input string.
 *
 * @param string $type Type of IDs being requested.
 * @param string $string Input string.
 * @return array List of matching IDs, or FALSE if not all matched.
 */
function pifr_server_environment_lookup($type, $string) {
  if (!$string) {
    return array();
  }

  $parts = array_map('trim', explode(',', $string));

  if ($type == 'client') {
    $result = db_query('SELECT client_id
                        FROM {pifr_client}
                        WHERE client_id IN (' . db_placeholders($parts, 'int') . ')
                        ORDER BY client_id', $parts);
  }
  elseif ($type == 'project') {
    $result = db_query("SELECT project_id
                        FROM {pifr_project}
                        WHERE name IN (" . db_placeholders($parts, 'text') . ")
                        ORDER BY name
                        LIMIT 10", $parts);
  }
  elseif ($type == 'branch') {
    $result = db_query("SELECT branch_id
                        FROM {pifr_branch}
                        WHERE client_identifier IN (" . db_placeholders($parts, 'text') . ")
                        ORDER BY client_identifier
                        LIMIT 10", $parts);
  }

  $ids = array();
  while ($id = db_result($result)) {
    $ids[$id] = $id;
  }
  return count($ids) == count($parts) ? $ids : FALSE;
}

/**
 * Format IDs as input string.
 *
 * @param string $type Type of IDs to be formatted.
 * @param array $data List of Ids to be formated.
 * @return string Formatted string.
 */
function pifr_server_environment_format($type, array $data) {
  if (!$data) {
    return '';
  }

  if ($type == 'client') {
    $result = db_query('SELECT client_id
                        FROM {pifr_client}
                        WHERE client_id IN (' . db_placeholders($data, 'int') . ')
                        ORDER BY client_id', $data);
  }
  elseif ($type == 'project') {
    $result = db_query("SELECT name
                        FROM {pifr_project}
                        WHERE project_id IN (" . db_placeholders($data, 'text') . ")
                        ORDER BY name
                        LIMIT 10", $data);
  }
  elseif ($type == 'branch') {
    $result = db_query("SELECT client_identifier
                        FROM {pifr_branch}
                        WHERE branch_id IN (" . db_placeholders($data, 'text') . ")
                        ORDER BY client_identifier
                        LIMIT 10", $data);
  }

  $values = array();
  while ($value = db_result($result)) {
    $values[] = check_plain($value);
  }
  return implode(', ', $values);
}

/**
 * Environment deletion confirmation form.
 */
function pifr_server_environment_delete_confirm_form(&$form_state, $environment_id) {
  $environment = pifr_server_environment_get($environment_id);
  if (!$environment) {
    drupal_set_message(t('Invalid environment.'), 'error');
    return array();
  }

  $form = array();
  $form['#redirect'] = 'admin/pifr/environment';
  $form['environment_id'] = array(
    '#type' => 'value',
    '#value' => $environment_id,
  );

  return confirm_form($form,
                      t('Are you sure you want to delete the @environment environment?', array('@environment' => $environment['title'])),
                      $form['#redirect']);
}

/**
 * Delete environment.
 */
function pifr_server_environment_delete_confirm_form_submit($form, &$form_state) {
  $environment = pifr_server_environment_get($form_state['values']['environment_id']);
  db_query('DELETE FROM {pifr_environment}
            WHERE environment_id = %d', $form_state['values']['environment_id']);
  drupal_set_message(t('@environment environment deleted', array('@environment' => $environment['title'])));
}

/**
 * Load an environment
 *
 * @param integer $environment_id Environment ID.
 * @return array Environment information.
 */
function pifr_server_environment_get($environment_id) {
  $result = db_query('SELECT *
                      FROM {pifr_environment}
                      WHERE environment_id = %d', $environment_id);
  return pifr_server_environment_load(db_fetch_array($result));
}

/**
 * Load all environments.
 *
 * @return array List of environments, each an array of environment
 *   information.
 * @see pifr_server_environment_get()
 */
function pifr_server_environment_get_all() {
  $result = db_query('SELECT environment_id
                      FROM {pifr_environment}
                      ORDER BY environment_id');

  $environments = array();
  while ($environment_id = db_result($result)) {
    $environments[$environment_id] = pifr_server_environment_get($environment_id);
  }
  return $environments;
}

/**
 * Get the relevant environments for a test.
 *
 * @param integer $test_id The test ID to get relevant environments for.
 * @return array List of environments keyed by the environment ID.
 */
function pifr_server_environment_test_get_all($test_id) {
  $result = db_query('SELECT environment_id
                      FROM {pifr_test_environment}
                      WHERE test_id = %d', $test_id);
  $environments = array();
  while ($environment_id = db_result($result)) {
    $environments[$environment_id] = pifr_server_environment_get($environment_id);
  }
  return $environments;
}

/**
 * Evaluate the environment criteria against a test and return the relvant.
 *
 * @param array $test Test information.
 * @return array List of relevant environments.
 */
function pifr_server_environment_test_evaluate(array $test) {
  // Get tree of test related information.
  if ($test['type'] == PIFR_SERVER_TEST_TYPE_CLIENT) {
    if ($client = pifr_server_client_get_test($test['test_id'])) {
      if ($client['type'] == PIFR_SERVER_CLIENT_TYPE_PROJECT) {
        // Project clients do not have any relevant environments.
        return array();
      }
      else {
        // The only environment(s) a test client test should run in is its own.
        $environments = array();
        foreach ($client['environment'] as $environment_id => $enabled) {
          if ($enabled) {
            $environments[$environment_id] = pifr_server_environment_get($environment_id);
          }
        }
        ksort($environments);
        return $environments;
      }
    }

    // Client record has not yet been saved.
    return array();
  }
  elseif ($test['type'] == PIFR_SERVER_TEST_TYPE_BRANCH) {
    $branch = pifr_server_branch_get_test($test['test_id']);
  }
  elseif ($test['type'] == PIFR_SERVER_TEST_TYPE_FILE) {
    $file = pifr_server_file_get_test($test['test_id']);
    $branch = pifr_server_branch_get($file['branch_id']);
  }
  if (empty($branch['plugin_argument']['test.php.version'])) {
    $branch['plugin_argument']['test.php.version'] = '5.3';
  }
  $project = pifr_server_project_get($branch['project_id']);
  $client = pifr_server_client_get($project['client_id']);

  // Cycle through all environments and find the relevant ones.
  $environments = pifr_server_environment_get_all();
  $relevant = array();
  foreach ($environments as $environment) {
    if (empty($environment['plugin_argument']['phpversion'])) {
      $environment['plugin_argument']['phpversion'] = '5.3';
    }
    if ((empty($environment['client']) || in_array($client['client_id'], $environment['client'])) &&
        (empty($environment['project']) || in_array($project['project_id'], $environment['project'])) &&
        (empty($environment['branch']) || in_array($branch['branch_id'], $environment['branch'])) &&
        (empty($environment['type']) || in_array($test['type'], $environment['type'])) &&
        ($environment['plugin_argument']['phpversion'] == $branch['plugin_argument']['test.php.version'])
    ) {
      $relevant[$environment['environment_id']] = $environment;
    }
  }

  // Ensure that the environments are always in the same order.
  ksort($relevant);

  return $relevant;
}

/**
 * Load environment information by preforming necessary data cleanups.
 *
 * @param array $environment Environment information.
 * @return array Cleaned up environment information.
 */
function pifr_server_environment_load($environment) {
  if ($environment) {
    foreach (array('client', 'project', 'branch', 'type', 'plugin_argument') as $key) {
      $environment[$key] = unserialize($environment[$key]);
    }
  }
  return $environment;
}

/**
 * Get the status of an environment in relation ot a test.
 *
 * @param integer $environment_id Environment ID.
 * @param integer $test_id Test ID.
 * @return array Environment status, one of the following.
 *   <code>
 *     array('status' => 'sent', 'client_id'),
 *     array('status' => 'result', 'result_id', 'code'),
 *     array('status' => 'queued'),
 *   </code>
 */
function pifr_server_environment_status_get($environment_id, $test_id) {
  $result = db_query('SELECT s.client_id, r.result_id, r.code
                      FROM {pifr_test} t
                      LEFT JOIN {pifr_environment_status} s
                        ON (t.test_id = s.test_id AND s.environment_id = %d)
                      LEFT JOIN {pifr_result} r
                        ON (t.test_id = r.test_id AND r.environment_id = %d)
                      WHERE t.test_id = %d', $environment_id, $environment_id, $test_id);
  $result = db_fetch_array($result);
  if ($result['result_id']) {
    return array('status' => 'result', 'result_id' => $result['result_id'], 'code' => $result['code']);
  }
  elseif ($result['client_id']) {
    return array('status' => 'sent', 'client_id' => $result['client_id']);
  }
  return array('status' => 'queued');
}

/*
 * Get the status of all environments related to a test.
 *
 * @param integer $test_id Test ID.
 * @return array Array of pifr_server_environment_status_get() statuses.
 */
function pifr_server_environment_status_get_all($test_id) {
  $status = array();
  $environments = pifr_server_environment_test_get_all($test_id);
  foreach ($environments as $environment) {
    $status[$environment[$environment_id]] = pifr_server_environment_status_get($environment_id, $test_id);
  }
  return $status;
}

/**
 * Get the environment ID that the client is currently testing.
 *
 * @param array $test Test information.
 * @param array $client Client information.
 * @return interger Environment ID, or FALSE.
 */
function pifr_server_environment_status_get_client(array $test, array $client) {
  $result = db_query('SELECT environment_id
                      FROM {pifr_environment_status}
                      WHERE test_id = %d
                      AND client_id = %d', $test['test_id'], $client['client_id']);
  return db_result($result);
}

/**
 * Get the current environment status for a client by client_id
 *
 * @param array $client Client information.
 *
 * @return interger Environment ID, or FALSE.
 */
function pifr_server_environment_status_get_by_clientid($client) {
  $result = db_query('SELECT *
                      FROM {pifr_environment_status}
                      WHERE client_id = %d', $client);
  $result = db_fetch_array($result);
  return $result;
}

/**
 * Get a list of all the plugins being used by the environments of the test.
 *
 * @param array $test Test information.
 * @return array Associative array of plugins.
 */
function pifr_server_environment_plugins(array $test) {
  $environments = pifr_server_environment_test_get_all($test['test_id']);
  $plugins = array();
  foreach ($environments as $environment) {
    $plugins[$environment['plugin']] = $environment['plugin'];
  }
  return $plugins;
}

/**
 * Reserve an environment for a particular test and client.
 *
 * @param integer $environment_id Environment ID.
 * @param integer $test_id Test ID.
 * @param integer $client_id Client ID.
 */
function pifr_server_environment_status_reserve($environment_id, $test_id, $client_id) {
  $exists = db_result(db_query('SELECT COUNT(test_id)
                                FROM {pifr_environment_status}
                                WHERE environment_id = %d AND test_id = %d',
                                $environment_id, $test_id));
  if (!$exists) {
    return db_query('INSERT INTO {pifr_environment_status}
                     (environment_id, test_id, client_id)
                     VALUES (%d, %d, %d)',
                     $environment_id, $test_id, $client_id);
  }
  else {
    return FALSE;
  }
}

/**
 * Clear a client environment status for a test.
 *
 * @param integer $client_id Client ID.
 */
function pifr_server_environment_status_clear_client($client_id) {
  db_query('DELETE FROM {pifr_environment_status}
            WHERE client_id = %d', $client_id);
}

/**
 * Clear the environment status for a test.
 *
 * @param integer $test_id Test ID.
 */
function pifr_server_environment_status_clear_all($test_id) {
  db_query('DELETE FROM {pifr_environment_status}
            WHERE test_id = %d', $test_id);
}

/**
 * Save an environment.
 *
 * If no environment exists the environment will be inserted, otherwise the
 * environment will be updated.
 *
 * @param array $environment Environment information, see the pifr_environment
 *   table schema for more information. If the environment ID is blank a new
 *   environment will be created.
 * @return Environment information after saving, if inserted it will include
 *   the new environment ID.
 */
function pifr_server_environment_save(array $environment) {
  if (!empty($environment['environment_id'])) {
    // Existing environment, update record.
    drupal_write_record('pifr_environment', $environment, 'environment_id');
  }
  else {
    // New environment, insert record.
    drupal_write_record('pifr_environment', $environment);
  }
  return $environment;
}

/**
 * Determine whether environment tests should be considered 'advisory'.
 *
 * Advisory tests are tests which may fail and return their results, but which
 * should not cause the overall branch/patch test to be considered a failure.
 * Environments are considered 'advisory' if the environment plugin_arguments
 * array contains an 'advisory' key with non-empty value.
 *
 * @param $environment A pre-loaded environment array.
 * @return boolean TRUE if considered 'advisory', otherwise FALSE
 */
function pifr_server_environment_is_advisory($environment) {
  return !empty($environment['plugin_argument']['advisory']);
}
