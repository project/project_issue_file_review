<?php

/**
 * @file
 * Provide test result functions.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Load a result.
 *
 * @param integer $result_id Result ID.
 * @return array Result information as described below.
 * <code>
 *   $result = array(
 *     'result_id',
 *     'test_id',
 *     'environment_id',
 *     'code',
 *     'details',
 *     'data', // Data loaded by review plugin.
 *   );
 * </code>
 */
function pifr_server_result_get($result_id) {
  $result = db_query('SELECT *
                      FROM {pifr_result}
                      WHERE result_id = %d', $result_id);
  $result = db_fetch_array($result);
  $result['details'] = unserialize($result['details']);

  // Load plugin data.
  $result = pifr_server_review_load($result);

  return $result;
}

/**
 * Load the result associated with a test and environment.
 *
 * @param interger $test_id Test ID.
 * @param interger $environment_id Environment ID.
 * @return array Result information, or FALSE.
 * @see pifr_server_result_get()
 */
function pifr_server_result_get_environment($test_id, $environment_id) {
  $result = db_query('SELECT result_id
                      FROM {pifr_result}
                      WHERE test_id = %d
                      AND environment_id = %d', $test_id, $environment_id);
  if ($result_id = db_result($result)) {
    return pifr_server_result_get($result_id);
  }
  return FALSE;
}

/**
 * Load all the results for a test.
 *
 * @param integer $test_id Test ID.
 * @return array List results keyed by environment ID.
 * @see pifr_server_result_get()
 */
function pifr_server_result_get_all($test_id) {
  $result_ids = db_query('SELECT result_id
                          FROM {pifr_result}
                          WHERE test_id = %d', $test_id);

  $results = array();
  while ($result_id = db_result($result_ids)) {
    $result = pifr_server_result_get($result_id);
    $results[$result['environment_id']] = $result;
  }
  return $results;
}

/**
 * Determine if a test passed or failed.
 *
 * @param integer $test_id Test ID.
 * @return boolean TRUE is pass, otherwise FALSE.
 */
function pifr_server_result_summary_status($test_id) {
  $pass = TRUE;
  $results = pifr_server_result_get_all($test_id);
  $environments = pifr_server_environment_test_get_all($test_id);
  $test = pifr_server_test_get($test_id);
  foreach ($results as $result) {
    // Check if an 'advisory' environment result.  If so, and there are
    // multiple environment results, then ignore the result when calculating
    // the overall test result.  Also ignore for client tests.
    if ((count($results) == 1)
       || (!pifr_server_environment_is_advisory($environments[$result['environment_id']]))
       || ($test['type'] == 1)
      ) {
      $pass = $pass && pifr_server_review_check($result);
    }
  }
  return $pass;
}

/**
 * Generate a summary message for a test.
 *
 * @param integer $test_id Test ID.
 * @return string Summary message.
 */
function pifr_server_result_summary_message($test_id) {
  // Cycle through all results and create a two level array structure with the
  // summary of each environment grouped by plugin.
  $summary = array();
  $pass = TRUE;
  $results = pifr_server_result_get_all($test_id);
  $environments = pifr_server_environment_test_get_all($test_id);
  foreach ($results as $result) {
    $summary_message = pifr_server_review_summary($result);
    $details = pifr_server_review_summary_format($result);

    $environment = $environments[$result['environment_id']];
    $summary[$environment['plugin']][] = '[' . check_plain($environment['title']) . '] ' . t($summary_message, $details);

    // Determine overall test status
    if (!pifr_server_environment_is_advisory($environment)) {
      $pass = $pass && pifr_server_review_check($result);
    }
  }

  // Condense the two level array into a summary string.
  $plugin_info = pifr_server_review_get_all();
  foreach ($summary as $plugin => $parts) {
    $summary[$plugin] = '[[' . $plugin_info[$plugin] . ']]: ' . implode(', ', $parts);
  }
  $summary = implode('; ', $summary) . '.';

  // Prepend overall summary status to summary.
  return ($pass ? t('PASSED') : t('FAILED')) . ': ' . $summary;
}

/**
 * Clear all test result records (includes tree).
 *
 * @param integer $test_id Test ID.
 * @see pifr_server_result_get().
 */
function pifr_server_result_clear($test_id) {
  $results = pifr_server_result_get_all($test_id);
  foreach ($results as $result) {
    pifr_server_result_delete($result);
  }
}

/**
 * Clear all test result records for a given environment.
 *
 * @param integer $test_id Test ID.
 * @see pifr_server_result_get().
 */
function pifr_server_result_environment_clear($test_id, $environment_id) {
  $results = pifr_server_result_get_all($test_id);
  if (!empty($results[$environment_id])) {
    pifr_server_result_delete($results[$environment_id]);
  }
}


/**
 * Save the specified result. The data will be saved in the three result
 * tables: pifr_result, pifr_result_detail, pifr_result_detail_assertion.
 *
 * @param array $result Result to save.
 * @return integer Result ID of saved result.
 */
function pifr_server_result_save(array $result) {
  drupal_write_record('pifr_result', $result);

  // Let plugins save additional data.
  pifr_server_review_record($result);

  return $result;
}

/**
 * Remove a test result.
 *
 * @param array $result Result information.
 */
function pifr_server_result_delete(array $result) {
  // Allow plugins to delete data before master result row is removed.
  pifr_server_review_delete($result);

  db_query('DELETE FROM {pifr_result} WHERE result_id = %d', $result['result_id']);
}
