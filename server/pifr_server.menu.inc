<?php
/**
 * @file
 * Provide menu items.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Menu items for hook_menu().
 */
function pifr_server_menu_items() {
  $items = array();

  // General information: all users.
  $items['pifr/status'] = array(
    'title' => 'Status',
    'page callback' => 'pifr_server_page_status',
    // Reverted from 'view all pifr tests' to allow security.drupal.org to
    // restrict access while allowing everyone to still see the front page.
    'access arguments' => array('access content'),
    'file' => 'pifr_server.page.inc'
  );
  $items['pifr/statistics'] = array(
    'title' => 'Statistics',
    // See pifr/status 'access argument' comment.
    'page callback' => 'pifr_server_page_statistics',
    'access arguments' => array('access content'),
    'file' => 'pifr_server.page.inc'
  );
  $items['pifr/test/%'] = array(
    'title' => 'View test',
    'page callback' => 'pifr_server_test_view',
    'page arguments' => array(2),
    'access callback' => 'pifr_server_test_access',
    'access arguments' => array(2),
    'type' => MENU_CALLBACK,
    'file' => 'pifr_server.test.inc'
  );
  $items['pifr/test/%/view'] = array(
    'title' => 'View',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );
  $items['pifr/test/%/tools'] = array(
    'title' => 'Tools',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pifr_server_test_tools_form', 2),
    'access arguments' => array('manage pifr tests'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'pifr_server.test.inc',
    'weight' => -9,
  );

  // Environment management: administrative.
  $items['admin/pifr/environment/add'] = array(
    'title' => 'Add',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pifr_server_environment_form'),
    'access arguments' => array('manage pifr environments'),
    'file' => 'pifr_server.environment.inc',
    'type' => MENU_LOCAL_TASK,
  );
  $items['admin/pifr/environment/edit/%'] = array(
    'title' => 'Environment',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pifr_server_environment_form', 4),
    'access arguments' => array('manage pifr environments'),
    'file' => 'pifr_server.environment.inc',
    'type' => MENU_CALLBACK,
  );
  $items['admin/pifr/environment/delete/%'] = array(
    'title' => 'Environment',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pifr_server_environment_delete_confirm_form', 4),
    'access arguments' => array('manage pifr environments'),
    'file' => 'pifr_server.environment.inc',
    'type' => MENU_CALLBACK,
  );
  $items['admin/pifr/environment/ahah/%/%'] = array(
    'title' => 'Environment',
    'page callback' => 'pifr_server_environment_ahah',
    'page arguments' => array(4, 5),
    'access arguments' => array('manage pifr environments'),
    'file' => 'pifr_server.environment.inc',
    'type' => MENU_CALLBACK,
  );
  $items['admin/pifr/environment/settings'] = array(
    'title' => 'Environment',
    'page callback' => 'pifr_server_environment_settings_ahah',
    'access arguments' => array('manage pifr environments'),
    'file' => 'pifr_server.environment.inc',
    'type' => MENU_CALLBACK,
  );

  // Client management: administrative.
  // Defined by view : admin/pifr/server.
  $items['admin/pifr/server/edit/%'] = array(
    'title' => 'Edit client',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pifr_server_manage_client_form', 4),
    'access arguments' => array('manage pifr clients'),
    'file' => 'pifr_server.manage.inc',
    'type' => MENU_CALLBACK,
  );
  $items['admin/pifr/server/enable/%'] = array(
    'title' => 'Request enable client',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pifr_server_client_enable_confirm_form', 4),
    'access arguments' => array('manage pifr clients'),
    'file' => 'pifr_server.manage.inc',
    'type' => MENU_CALLBACK,
  );
  $items['admin/pifr/server/disable/%'] = array(
    'title' => 'Request enable client',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pifr_server_client_disable_confirm_form', 4),
    'access arguments' => array('manage pifr clients'),
    'file' => 'pifr_server.manage.inc',
    'type' => MENU_CALLBACK,
  );
  $items['admin/pifr/server/remove/%'] = array(
    'title' => 'Remove client',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pifr_server_client_remove_confirm_form', 4),
    'access arguments' => array('manage pifr clients'),
    'file' => 'pifr_server.manage.inc',
    'type' => MENU_CALLBACK,
  );

  // Client management: user specific.
  $items['user/%user/pifr'] = array(
    'title' => 'PIFR Clients',
    'page callback' => 'pifr_server_manage_dashboard',
    'page arguments' => array(1),
    'access arguments' => array('manage own pifr client'),
    'file' => 'pifr_server.manage.inc',
    'type' => MENU_LOCAL_TASK
  );
  $items['user/%user/pifr/dashboard'] = array(
    'title' => 'Dashboard',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10
  );
  $items['user/%user/pifr/add'] = array(
    'title' => 'Add client',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pifr_server_manage_client_form', NULL, 1),
    'access callback' => 'pifr_server_access',
    'access arguments' => array('add pifr project client', 'add pifr test client'),
    'file' => 'pifr_server.manage.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => -9
  );
  $items['user/%user/pifr/edit/%'] = array(
    'title' => 'Edit client',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pifr_server_manage_client_form', 4, 1),
    'access callback' => 'pifr_server_manage_access',
    'access arguments' => array(4, 1),
    'file' => 'pifr_server.manage.inc',
    'type' => MENU_CALLBACK,
  );
  $items['user/%user/pifr/enable/%'] = array(
    'title' => 'Request enable client',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pifr_server_client_enable_confirm_form', 4, 1),
    'access callback' => 'pifr_server_manage_access',
    'access arguments' => array(4, 1),
    'file' => 'pifr_server.manage.inc',
    'type' => MENU_CALLBACK,
  );
  $items['user/%user/pifr/disable/%'] = array(
    'title' => 'Disable client',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pifr_server_client_disable_confirm_form', 4, 1),
    'access callback' => 'pifr_server_manage_access',
    'access arguments' => array(4, 1),
    'file' => 'pifr_server.manage.inc',
    'type' => MENU_CALLBACK,
  );
  $items['user/%user/pifr/remove/%'] = array(
    'title' => 'Remove client',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('pifr_server_client_remove_confirm_form', 4, 1),
    'access callback' => 'pifr_server_manage_access',
    'access arguments' => array(4, 1),
    'file' => 'pifr_server.manage.inc',
    'type' => MENU_CALLBACK,
  );

  return $items;
}
