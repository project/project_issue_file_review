<?php
/**
 * @file
 * Provide cron functions.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

function pifr_client_cron_run() {
  if (pifr_client_cron_is_busy() && time() > (variable_get('pifr_client_test_start', 0) + (variable_get('pifr_client_timeout', 45) * 60))) {
    // The test has been running longer than the timeout allows, so it is most
    // likely hung.  Reset the testbot client to prepare for next test.
    $test = variable_get('pifr_client_test', FALSE);
    // Check to ensure we actually have a test to reset
    if (!empty($test) && variable_get('pifr_client_test_start', 0) != 0) {
      // Reset the pifr_client_test and pifr_client_test_start variables.
      watchdog('pifr_client', 'Reset client due to test timeout (t: @test_id).', array('@test_id' => $test['test_id']));
      pifr_client_cron_clear_variables();
      // Kill any active run_tests.sh or curl processes.
      pifr_client_cron_test_processes_kill(pifr_client_cron_test_processes_get());
      // We should probably clean out any environment setup processes too, but
      // they are less likely to still be active at the test timeout value, and
      // should not have made it through the pifr_client_cron_is_busy() check.
    }
  }

  if (!pifr_client_cron_is_busy()) {
    // If the testbot is not currently active, then request the next test. The
    // is_busy check will return TRUE if we see an active git or php process,
    // or a 'pifr_client_requesting' variable; so that we do not call this a
    // second time during environment setup.
    pifr_client_cron_request_next();
  }
  else {
    // Do not report status during environment status, since the status could
    // trigger a cancel request back from the PIFR server, and cancelling
    // during environment setup is more hassle than it's worth due to the many
    // varied states the environment could be in.  Instead, we wait until the
    // environment setup has completed and actual testing has begun, at which
    // point the testbot will be in a well-known state.
    $pids = pifr_client_cron_test_processes_get();
    if (!empty($pids)) {
      pifr_client_cron_report_status();
    }
  }
}

function pifr_client_cron_report_status() {
  module_load_include('xmlrpc.inc', 'pifr_client');
  if ($op = pifr_client_xmlrpc_report_status()) {
    if (isset($op['command']) && $op['command'] == 'reset') {
      pifr_client_cron_reset();
      watchdog('pifr_client', 'Client reset in response to server request.');
    }
  }
}

function pifr_client_cron_request_next() {
  // We have a small race condition while PIFR client requests the next test
  // from the PIFR server, where the pifr_client_test variable is false and
  // pifr_client_test_start is not set.  If cron runs during this time, it may
  // result in pifr_client_cron_is_busy() being FALSE, triggering a second call
  // to this function. To close this race condition window, we add a flag to
  // track the 'requesting' state; and check it in pifr_client_cron_is_busy().
  variable_set('pifr_client_requesting', TRUE);
  // Now load the appropriate includes and request the next test.
  module_load_include('xmlrpc.inc', 'pifr_client');
  if ($test = pifr_client_xmlrpc_request_next()) {
    // Test found, save it in the pifr_client_test variable.
    variable_set('pifr_client_test',  $test);
    // Since this satisfies the 'pifr_client_cron_is_busy()' test, clear the
    // requesting flag.
    variable_set('pifr_client_requesting', FALSE);
    // Hit the review URL to trigger testing.
    $debug_options = array();
    if (variable_get('pifr_xdebug_session', FALSE)) {
      $debug_options = array('query' => "XDEBUG_SESSION_START=" . variable_get('pifr_xdebug_session', ""));
    }
    pifr_client_cron_exec_background('curl --insecure ' . url('pifr/client/review', array('absolute' => TRUE) + $debug_options ));
    return TRUE;
  }
  // No test found, clear our 'requesting' flag.
  variable_set('pifr_client_requesting', FALSE);
  return FALSE;
}

function pifr_client_cron_is_busy() {
  // Check the 'pifr_client_test' variable for an active test
  return variable_get('pifr_client_test', FALSE)
    // Check for a pifr_client_request_next() call in progress
    || variable_get('pifr_client_requesting', FALSE)
    // Check for active run-tests.php or curl processes
    || pifr_client_cron_test_processes_get()
    // Check for active 'php -l -f' syntax check processes
    || pifr_client_cron_syntax_processes_get()
    // Check for active 'git clone' processes
    || pifr_client_cron_git_processes_get();
}

function pifr_client_cron_reset() {
  pifr_client_cron_clear_variables();
  pifr_client_cron_test_processes_kill(pifr_client_cron_test_processes_get());
}

function pifr_client_cron_clear_variables() {
  variable_del('pifr_client_test');
  variable_del('pifr_client_test_start');
}

/**
 * Find any PHP processes that are running the tests.
 *
 * @return array List of process IDs (pids).
 */
function pifr_client_cron_test_processes_get() {
  $pids = array();

  exec('ps aux | grep php', $output);
  foreach ($output as $line) {
    if (preg_match('/^.*? (\d+ ).*?(php \\.\\/(core\\/)?scripts\\/run-tests\\.sh|curl.*?pifr\\/client\\/review)/m', $line, $match)) {
      // Processes running:
      //   "php ./scripts/run-tests.sh",
      //   "curl http://example.com/pifr/client/review"
      $pids[] = $match[1];
    }
  }

  return $pids;
}

/**
 * Find any PHP processes that are running the tests.
 *
 * @return array List of process IDs (pids).
 */
function pifr_client_cron_syntax_processes_get() {
  $pids = array();

  exec('ps aux | grep php', $output);
  foreach ($output as $line) {
    if (preg_match('/^.*? (\d+ ).*?(php -l -f )/m', $line, $match)) {
      // Processes running:
      //   "php -l -f '[filename]'".
      $pids[] = $match[1];
    }
  }

  return $pids;
}

/**
 * Find any GIT processes that are running for tests.
 *
 * @return array List of process IDs (pids).
 */
function pifr_client_cron_git_processes_get() {
  $pids = array();

  exec('ps aux | grep git', $output);
  foreach ($output as $line) {
    if (preg_match('/^.*? (\d+ ).*?(git clone .*? checkout --reference \\/var\\/cache\\/git\\/reference)/m', $line, $match)) {
      // Process running: "git clone xxxxxx checkout --reference /var/cache/git/reference".
      $pids[] = $match[1];
    }
  }

  return $pids;
}

/**
 * Kill the specified list of processes.
 *
 * @param array $pids List of process IDs (pids).
 */
function pifr_client_cron_test_processes_kill($pids) {
  foreach ($pids as $pid) {
    exec('kill ' . $pid);
  }
}

/**
 * Execute a system command in the background.
 *
 * @return integer proccess ID.
 */
function pifr_client_cron_exec_background($command) {
  return shell_exec("nohup $command > /dev/null 2> /dev/null & echo $!");
}
