<?php

/**
 * @file
 * Git implementation of review interface.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Git implementation of VCS interface.
 */
class pifr_client_vcs_git extends pifr_client_vcs {

  /**
   * Array of patch levels to try when patching.
   */
  protected $patch_levels_to_try = array(1, 0);

  /**
   * The patch level (-p0 or -p1) detected.
   */
  protected $patch_level = 0;

  /**
   * Array of files that should be checked after patch application.
   */
  protected $files_affected_by_patch = array();


  /**
   * Check out a given repository URL into given directory.
   *
   * @param $directory
   *   Directory to check out into. If empty, the name of the project (derived
   *   from the url) is used.
   * @param unknown_type $url
   *   Repo URL, like git://git.drupal.org/project/drupal.git
   * @param $vcs_id
   *   branch or tag to check out, like 7.x-1.0, 7.x, or 7.x-1.x.
   *
   * @return
   *   TRUE on success, FALSE on failure.
   */
  public function checkout($work_tree, $url, $vcs_id) {
    $url = escapeshellarg($url);
    $vcs_id = escapeshellarg($vcs_id);

    // If $work_tree is empty, we assume we're in the directory where the
    // clone is to happen.
    if (empty($work_tree)) {
      $work_tree = preg_replace('%^.*/([^/]*)\.git.*%', '$1', $url);
      $work_tree = getcwd() . '/' . $work_tree;
    }
    $git_dir = escapeshellarg("$work_tree/.git");
    $work_tree = escapeshellarg($work_tree);

    // We can use a reference repository to shortcut the wait for a clone.
    // It can have any or all of the projects that might get checked out.
    // This should be an absolute path to the reference repo or a git URL.
    $reference_repo = variable_get('pifr_reference_repository', '');

    // Note that we may get a warning if the repo doesn't have a HEAD pointing
    // to a valid branch (as in doesn't have a master branch). Should not be
    // a failure though.
    $cmd = "git clone $url $work_tree";
    if (!empty($reference_repo)) {
      $cmd .= " --reference $reference_repo";
    }
    if (!pifr_client_review::exec($cmd)) {
      return FALSE;
    }
    // "git checkout <vcs_id>" should work for branches or tags for modern
    // versions of git.
    $cmd = "git --work-tree=$work_tree --git-dir=$git_dir checkout $vcs_id";

    $result = pifr_client_review::exec($cmd);
    $cmd = "git --work-tree=$work_tree --git-dir=$git_dir status";
    pifr_client_review::exec($cmd);
    $cmd = "git --work-tree=$work_tree --git-dir=$git_dir log --oneline -1";
    $output = NULL;
    pifr_client_review::exec($cmd, TRUE, $output, TRUE);
    return $result;  // From the checkout.
  }

  public function annotate($file) {
    $lines = pifr_client_review::exec_output("git annotate $file");

    $map = array();
    $number = 1;
    foreach ($lines as $line) {
      if (preg_match('/.*?\s+\(\s+(.*?)\s+.*?\)/', $line, $match)) {
        $map[$number++] = $match[1];
      }
    }
    return $map;
  }

  public function commit_id() {
    $lines = pifr_client_review::exec_output('git log -n 1');

    foreach ($lines as $line) {
      if (preg_match('/^commit (.*?)$/m', $line, $match)) {
        return $match[1];
      }
    }
    return FALSE;
  }

  /**
   * Apply a patch using git apply.
   *
   * @param $patch
   *   The patch file to apply.
   * @param $level
   *   The patch level to use. Normal is -p1.
   * @param $check_only
   *   If TRUE, do not attempt to patch, just see if it applies.
   *
   * @return
   *   TRUE on successful patching or successful check, FALSE otherwise.
   */
  public function apply($patch, $patch_level = 1, $check_only = FALSE) {
    $test_result = pifr_client_review::exec("git apply --check -p$patch_level $patch", TRUE);
    if ($check_only) {
      return ($test_result == 0);
    }
    if ($test_result) {
      $this->patch_level = $patch_level;
      $git_output = "";
      $git_command_result = pifr_client_review::exec("git apply -v -p$patch_level $patch", TRUE, $git_output);
      $this->files_affected_by_patch = $this->determine_files_affected_by_patch($git_output);
      return TRUE;
    }
    return FALSE;
}

  /**
   * Given output from a git patch application, determine what files changed.
   *
   * The git apply -v output looks like this:
   *   Applied patch junkincludes/token.inc cleanly.
   *   Applied patch includes/database/prefetch.inc => junkincludes/database/prefetch.inc cleanly.
   * The regular expression is used to capture the destination filename.
   * Then we check for existence (as git may list files that no longer exist).
   * Finally, return the list of files affected by the patch.
   *
   * @param $string
   *   The output from the git apply -v command.
   * @return
   *   Array containing the files affected by the patch.
   */
  public function determine_files_affected_by_patch($string) {
    $regex = '%Applied\spatch\s*  # Key start of string
             (?:.*=>\s*)?         # Skip optional renames that end in =>
             (.*?)                # Now capture the filename (non-greedy)
             \s*cleanly\.         # Finally match the ending "cleanly."
             %x';
    $num_matches = preg_match_all($regex, $string, $matches);
    $files = $matches[1];
    foreach ($files as $index => $filename) {
      if (!file_exists($filename)) {
        unset($files[$index]);
      }
    }
    return $files;
  }

  /**
   * Inspect the patch to determine which files should have changed.
   *
   * @param $patch
   *   The patch which was applied. (It has to have already been applied to
   *   initialize $this->patch_level.
   */
  public function changed_files($patch) {
    return $this->files_affected_by_patch;
  }

  public function get_display($url, $branch) {
    return $url . ' (' . $branch . ')';
  }
}

