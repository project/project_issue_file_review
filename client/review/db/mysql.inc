<?php
/**
 * @file
 * MySQL implementation of review interface.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * MySQL implementation of review interface.
 */
class pifr_client_db_interface_mysql implements pifr_client_db_interface {

  function get_information() {
    global $db_url;

    $url = parse_url($db_url['pifr_checkout']);
    $db = array();
    $db['name'] = substr(urldecode($url['path']), 1);
    $db['username'] = urldecode($url['user']);
    $db['password'] = urldecode($url['pass']);
    $db['host'] = urldecode($url['host']);
    return $db;
  }

  public function create_database() {
    global $db_url;
    $database = substr(parse_url($db_url['pifr_checkout'], PHP_URL_PATH), 1);
    return (bool) db_query("CREATE DATABASE `%s`", $database);
  }

  public function drop_database() {
    global $db_url;
    $database = substr(parse_url($db_url['pifr_checkout'], PHP_URL_PATH), 1);
    return (bool) db_query("DROP DATABASE IF EXISTS `%s`", $database);
  }

  public function query($sql) {
    if (db_set_active('pifr_checkout')) {
      $result = @db_query($sql);
      if (!$result) {
      	db_set_active();
        watchdog('pifr', 'Failed query %query', array('%query' => $sql));
      	return FALSE;
      }
      if ($result === TRUE) {
        db_set_active();
        return TRUE;
      }
      $rows = array();
      while ($row = db_fetch_array($result)) {
        $rows[] = $row;
      }
      db_set_active();
      return $rows;
    }
    return FALSE;
  }

  public function import($file) {
    $info = $this->get_information();
    if (pifr_client_review::exec("mysql --user={$info['username']} --password={$info['password']} {$info['name']} < $file")) {
      return count($this->query('SHOW TABLES'));
    }
    return FALSE;
  }
}
