<?php
/**
 * @file
 * PostgreSQL implementation of review interface.
 *
 * @author Josh Waihi ("fiasco", http://drupal.org/user/188162)
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 * @author Neil Bertram ("neilnz", http://drupal.org/user/284465)
 */

/**
 * PostgreSQL implementation of review interface.
 */
class pifr_client_db_interface_pgsql implements pifr_client_db_interface {

  // Connection to the checked out database.
  protected $pdo;

  // Connection to the PostgreSQL database.
  // Sudo connection for creating/dropping databases, as it's not possible to drop a database through an active connection
  protected $sudo_conn;

  public function __construct() {
    $info = $this->get_information();
    $this->sudo_conn = new PDO('pgsql:dbname=postgres;host=' . $info['host'], $info['username'], $info['password']);
  }

  protected function pg_conn() {
    $info = $this->get_information();
    if (!$this->pdo) {
      $this->pdo = new PDO('pgsql:dbname=' . $info['name'] . ';host=' . $info['host'], $info['username'], $info['password']);
    }
    return $this->pdo;
  }

  public function get_information() {
    global $db_url;

    $url = parse_url($db_url['pifr_checkout']);
    $db = array();
    $db['name'] = substr(urldecode($url['path']), 1);
    $db['username'] = urldecode($url['user']);
    $db['password'] = urldecode($url['pass']);
    $db['host'] = urldecode($url['host']);
    return $db;
  }

  public function create_database() {
    global $db_url;
    $database = substr(parse_url($db_url['pifr_checkout'], PHP_URL_PATH), 1);
    return $this->sudo_conn->query('CREATE DATABASE "' . $database . '" WITH ENCODING \'UNICODE\'');
  }

  public function drop_database() {
    global $db_url;
    $database = substr(parse_url($db_url['pifr_checkout'], PHP_URL_PATH), 1);
    return $this->sudo_conn->query('DROP DATABASE "' . $database . '"');
  }

  public function query($sql, $args = array()) {
    try {
      if (empty($args)) {
        $rs = $this->pg_conn()->query(db_prefix_tables($sql));
      }
      else {
        $rs = $this->pg_conn()
          ->prepare(db_prefix_tables($sql))
          ->execute($args);
      }
      return is_object($rs) ? $rs->fetchAll() : $rs;
    }
    catch (Exception $e) {
      return FALSE;
    }
  }

  public function import($file) {
    if ($this->query(file_get_contents($file))) {
      $information = $this->get_information();
      $database = $information['name'];
      $tables = $this->query("SELECT table_name FROM information_schema.tables WHERE (table_schema = '$database' OR table_catalog = '$database')");
      return count($tables);
    }
    return FALSE;
  }
}

