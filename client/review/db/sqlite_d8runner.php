<?php
/**
 * @file
 * SQLite implementation for querying Drupal 8's sqlite test runner.
 */
require_once 'sqlite.inc';

/**
 * SQLite implementation for querying Drupal 8's sqlite test runner.
 */
class pifr_client_db_interface_sqlite_d8runner extends pifr_client_db_interface_sqlite {

  /**
   * @var \PDO
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public function get_information() {
    throw new \Exception('Calling get_information is invalid.');
  }

  /**
   * {@inheritdoc}
   */
  public function create_database() {
    throw new \Exception('Calling create_database is invalid.');
  }

  /**
   * {@inheritdoc}
   */
  public function drop_database() {
    throw new \Exception('Calling drop_database is invalid.');
  }

  /**
   * {@inheritdoc}
   */
  public function query($sql) {
    if ($connection = $this->connection()) {
      try {
        // It is impossible for the tables to be prefixed.
        $sql = strtr($sql, array('{' => '', '}' => ''));
        $statement = $connection->prepare($sql);
        $statement->execute();
        return $statement->fetchAll(PDO::FETCH_ASSOC);
      }
      catch (PDOException $e) {
        return array();
      }
    }
    return array();
  }

  /**
   * {@inheritdoc}
   */
  public function import($file) {
    throw new \Exception('Calling import is invalid.');
  }

  /**
   * Return a connection to the D8 test result SQLite database.
   *
   * We use one connection per query. Because we can, and to avoid having to do
   * crazy exception handling.
   */
  protected function connection() {
    if (!$this->connection) {
      $this->connection = new PDO('sqlite:/tmpfs/pifr/test.sqlite', NULL, NULL, array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
    }
    return $this->connection;
  }

}
