<?php

/**
 * @file
 * Provide XML-RPC callbacks and invoke functions.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Retrieve the PIFR version string.
 *
 * @return string PIFR version string.
 */
function pifr_client_xmlrpc_version() {
  $info = parse_ini_file(drupal_get_path('module', 'pifr') . '/pifr.info');
  return (string) (isset($info['version']) ? $info['version'] : 'unknown');
}

/**
 * Report the current testing status to the server
 *
 * @return array Server command, or FALSE
 */
function pifr_client_xmlrpc_report_status() {
  // Prepare the status report, with current test ID and environment
  $test = variable_get('pifr_client_test', FALSE);
  if (!$test) {
    return FALSE;
  }
  $status = array('test_id' => $test['test_id'], 'environment' => $test['review']['plugin']);

  $response = xmlrpc(PIFR_CLIENT_SERVER . 'xmlrpc.php', 'pifr.status', PIFR_CLIENT_KEY, $status);

  if ($response === FALSE) {
    watchdog('pifr_client', 'Failed to report testing status: @message', array('@message' => xmlrpc_error_msg()), WATCHDOG_ERROR);
    return FALSE;
  }
  else if ($response == PIFR_RESPONSE_RESET) {
    // Server is requesting the client reset testing.  Format command and pass
    // on to pifr_client_cron_report_status().
    return array('command' => 'reset');
  }
  else if ($response != PIFR_RESPONSE_ACCEPTED) {
    watchdog('pifr_client', 'When reporting status, server responded: @message',
                      array('@message' => pifr_response_code($response)), WATCHDOG_NOTICE);
    return FALSE;
  }
  // Report accepted, but we don't have any follow-up action, so return FALSE.
  return FALSE;
}

/**
 * Request the next test from the server.
 *
 * @return array Test array, or FALSE.
 */
function pifr_client_xmlrpc_request_next() {
  $response = xmlrpc(PIFR_CLIENT_SERVER . 'xmlrpc.php', 'pifr.next', PIFR_CLIENT_KEY);

  if ($response === FALSE) {
    watchdog('pifr_client', 'Failed to request next test: @message', array('@message' => xmlrpc_error_msg()), WATCHDOG_ERROR);
    return FALSE;
  }
  else if (isset($response['response'])) {
    watchdog('pifr_client', 'When requesting next test, server responded: @message',
                      array('@message' => pifr_response_code($response['response'])), WATCHDOG_NOTICE);
    // Check the 'auto disable on error' setting
    if (variable_get('pifr_auto_disable_on_error', 0)) {
      // If rejected by the server (due to an invalid key or other issue),
      // disable the testbot to avoid filling the watchdog log with invalid
      // responses every minute. The testbot will need to be manually
      // re-enabled once the error condition has been cleared.
      if ($response['response'] == PIFR_RESPONSE_INVALID_SERVER) {
        variable_set('pifr_active', !PIFR_ACTIVE);
      }
    }
    return FALSE;
  }
  else if (!$response) {
    // Empty array, no tests.
    return FALSE;
  }
  pifr_debug('Requested and received test from server: XML-RPC pifr.next() response=<pre>%response</pre>', array('%response' => var_export($response, TRUE)));
  return $response;
}

/**
 * Send the results of review to the server.
 *
 * @param array $result Result information.
 * @return boolean TRUE if succesful, otherwise FALSE.
 */
function pifr_client_xmlrpc_send_result(array $result) {
  $compressed_result = base64_encode(gzcompress(serialize($result), 9));

  $response = xmlrpc(PIFR_CLIENT_SERVER . 'xmlrpc.php', 'pifr.result', PIFR_CLIENT_KEY, $compressed_result);

  if ($response === FALSE) {
    watchdog('pifr_client', 'Failed to send result: @message', array('@message' => xmlrpc_error_msg()), WATCHDOG_ERROR);
    return FALSE;
  }
  else if ($response != PIFR_RESPONSE_ACCEPTED) {
    watchdog('pifr_client', 'When sending result, server responded: @message',
                      array('@message' => pifr_response_code($response)), WATCHDOG_NOTICE);
    return FALSE;
  }

  watchdog('pifr_client', 'Test results successfully sent: (t: @test_id).', array('@test_id' => $result['test_id']));
  pifr_debug('Sent result to PIFR server; result=<pre>%result</pre>', array('%result' => var_export($result, TRUE)));

  return TRUE;
}
