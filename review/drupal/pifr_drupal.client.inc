<?php

/**
 * @file
 * Provide Drupal client review base implementation.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Drupal client review base implementation.
 */
class pifr_client_review_pifr_drupal extends pifr_client_review {

  /**
   * Default Drupal core repository URL.
   * @todo: We will be testing more than one branch of core shortly!
   *
   */
  protected $core_url = 'git://git.drupal.org/project/drupal.git';

  /**
   * Location of default sites module directory.
   */
  protected $module_directory = 'sites/default/modules';

  /**
   * Location of project being tested.
   *
   * @var string
   */
  protected $project_directory;

  /**
   * Analyze review information.
   */
  public function __construct(array $test) {
    parent::__construct($test);

    $this->core_url = variable_get('pifr_drupal_repository', 'git://git.drupal.org/project/drupal.git');
    $this->admin_username = $test['review']['argument']['drupal.user'];
    $this->admin_password = !empty($test['review']['argument']['drupal.pass']) ? $test['review']['argument']['drupal.pass'] : 'drupal';
    // Allow the Drupal core URL to be overridden via an argument.
    if (!empty($this->test['review']['argument']['drupal.core.url'])) {
      $this->core_url = $this->test['review']['argument']['drupal.core.url'];
    }

    pifr_debug('Test(%test_id) request received: <pre>%test</pre>', array('%test_id' => $this->test['test_id'], '%test' => print_r($test, TRUE)));
    $this->log('core_url = [' . $this->core_url . '];  Test repository = [' . $this->test['vcs']['main']['repository']['url'] . ']');



    if ($this->test['review']['plugin'] == 'pifr_simpletest') {
      if ($this->test['vcs']['main']['repository']['url'] != $this->core_url) {
        // Main repository is not core, so this must be a module test. Move the
        // module repository (main) to the dependencies and find the core
        // repository in the dependencies and move it to main.
        $this->test['vcs']['dependencies'][] = $this->test['vcs']['main'];

        // Store the project name 'simpletest' for use when applying patches.
        $this->project_directory = basename($this->test['vcs']['main']['repository']['url']);
        $this->project_directory = preg_replace('/\.git$/', '', $this->project_directory);
        $this->log('Project directory is [' . $this->project_directory . ']');


        foreach ($this->test['vcs']['dependencies'] as $key => $dependency) {
          if ($dependency['repository']['url'] == $this->arguments['drupal.core.url']) {
            $this->test['vcs']['main'] = $dependency;
            unset($this->test['vcs']['dependencies'][$key]);
            break;
          }
        }
      }

      // Add SimpleTest as dependencies, unless it has already been added.
      if ($this->test['review']['argument']['drupal.core.version'] == 6) {
        $simpletest_url = variable_get('pifr_simpletest_repository', 'git://git.drupal.org/project/simpletest.git');
        $found = FALSE;
        foreach ($this->test['vcs']['dependencies'] as $key => $dependency) {
          if ($dependency['repository']['url'] == $simpletest_url) {
            $found = TRUE;
            break;
          }
        }

        if (!$found) {
          // Since SimpleTest is not included in Drupal 6 it needs to be checked
          // out from contrib.
          $this->test['vcs']['dependencies'][] = array(
            'repository' => array(
              'type' => 'git',
              'url' => $simpletest_url,
            ),
            'vcs_identifier' => '6.x-2.x',
          );
        }
      }

      // Set the dependency directory.
      $this->arguments['test.directory.dependency'] = $this->module_directory;

      // If the main project is not Drupal core then all patches should be
      // applied from the root of the primary dependency or project directory.
      if (!empty($this->project_directory)) {
        $this->arguments['test.directory.apply'] = $this->module_directory . '/' . $this->project_directory;
      }
    }
  }


  /**
   * Get the relevant files to check for syntax.
   *
   * If patches and dependencies are being reviewed then make paths relative to modules.
   */
  protected function syntax_files() {
    // If no files to apply then attempt to filter files by arguments.
    if (!$this->apply_files()) {
      if (!empty($this->arguments['drupal.modules'])) {
        $files = array();
        foreach ($this->arguments['drupal.modules'] as $module) {
          $path = $this->module_path($module);
          if ($path !== FALSE) {
            if (!empty($path)) {
              $scan = file_scan_directory($this->checkout_directory . '/' . $path, '.*');
            }
            else {
              $scan = file_scan_directory($this->checkout_directory, '.*');
            }
            foreach ($scan as $file) {
              $files[] = str_replace($this->checkout_directory . '/', '', $file->filename);
            }
          }
        }
        return $files;
      }
      elseif (!empty($this->test['review']['argument']['directory'])) {
        $scan = file_scan_directory($this->checkout_directory . '/' . $this->test['review']['argument']['directory'], '.*');
        $files = array();
        foreach ($scan as $file) {
          $files[] = str_replace($this->checkout_directory . '/', '', $file->filename);
        }
        return $files;
      }
    }

    $files = parent::syntax_files();

    // If there are patch files and dependencies then assume the patch files
    // are relative to the modules.
    if (!empty($this->test['files']) && !empty($this->test['vcs']['dependencies'])) {
      foreach ($files as &$file) {
        // Ensure module directory hasn't already been prepended!
	// Don't apply for coder tests, as they take place in checkout directly
        if (($this->test['review']['plugin'] != 'pifr_coder') && (strpos($file, $this->module_directory) === FALSE)) {
          $file = $this->module_directory . '/' . $file;
        }
      }
    }

    return $files;
  }

  /**
   * Ingore non-PHP files during syntax check.
   */
  protected function syntax_ignore(array $files) {
    $php_extensions = array('php', 'inc', 'install', 'module', 'test');
    foreach ($files as $key => $file) {
      if (!in_array(pathinfo($file, PATHINFO_EXTENSION), $php_extensions)) {
        unset($files[$key]);
      }
    }

    // We remove the D8 vendor/ and core/vendor directories. They contain
    // non-Drupal code which we do not want to run through coder. Due to version
    // incompatibilities, PHP lint results of vendor code will likely cause
    // false failures.
    // @TODO: Modules can still specify a requirement for PHP 5.4, in which
    // case they may still fail the syntax checks.
    foreach ($files as $key => $file) {
      if (preg_match('#^core/vendor/|^vendor/#', $file)) {
        unset($files[$key]);
      }
    }
    return $files;
  }

  /**
   * Check syntax using PHP parser.
   */
  protected function syntax_check($file) {
    return $this->exec('php -l -f ' . escapeshellarg('./' . $file));
  }

  /**
   * Run the Drupal installer.
   */
  protected function install() {
    global $base_url, $base_path;

    // Use paths relative to checkout directory so that SimpleTest browser will function.
    $base_url .= '/checkout';
    $base_path = '/checkout';

    // Drupal 8 does not need a parent/test-runner site.
    if ($this->arguments['drupal.core.version'] >= 8) {
      return;
    }

    // Since the drush installation technique doesn't break the confirmation
    // install.patch, we'll pretend to break just because we got it. This
    // can be improved by changing the *server* logic and patch.
    $matches = preg_grep('/install\.patch/', $this->test['files']);
    if (!empty($matches)) {
      $this->set_error(array('@reason' => t('install.patch does not take drush installation into account, so failing deliberately')));
      return;
    }

    $db_info = $this->database->get_information();
    $db_type = pifr_client_review_database_type();
    $db_url = $db_type . "://";
    // Only include authorization credentials if defined
    if (!empty($db_info['username'])) {
      $db_url .= $db_info['username'];
      if (!empty($db_info['password'])) {
        $db_url .= ':' . $db_info['password'];
      }
    }
    // Sqlite install does not use the server name
    if ($db_type != 'sqlite') {
      $db_url .= '@localhost/';
    }
    else {
      // SQLITE HACK: Drush processes the db_url parameter from whatever
      // directory the command is executed in. We're passing a directory path,
      // relative to the server root, but it will be executed from the checkout
      // directory; so we need to normalize the db_url parameter.
      // TODO: This is really ugly ... find another way.
      // /var/lib/drupaltestbot/sites/default/files/checkout
      $db_url .= '../../../../../../..';
    }
    // Append the database name
    $db_url .= $db_info['name'];

    // Drush 6.1.0 doesn't seem to work with -r, so set dir and then reset it.
    $cwd = getcwd();
    // Drush site install for Drupal 8 does not support the clean urls argument.
    $extra_args = '';
    if ($this->test['review']['argument']['drupal.core.version'] < 8) {
      $extra_args = '--clean-url=0';
    }
    $drush_command = "cd {$this->checkout_directory} && drush si -y --db-url={$db_url} {$extra_args} --account-name={$this->admin_username} --account-pass={$this->admin_password} --account-mail=admin@example.com";
    $si_output = "";
    $this->log("drush si command={$drush_command}");
    $si_result = $this->exec($drush_command, TRUE, $si_output, TRUE);
    $this->exec("cd {$cwd}");
    if ($si_result !== TRUE) {
      $this->set_error(array('@reason' => "{$drush_command} failed with return code $si_result, output={$si_output}"));
      return;
    }

    // Set Drupal variables if specified.
    if (!empty($this->arguments['drupal.variables'])) {
      $this->install_variables($this->arguments['drupal.variables']);
    }

    return;
  }

  /**
   * Set Drupal variables.
   *
   * @param array $variables List of variables in the format key:value.
   */
  protected function install_variables(array $variables) {
    foreach ($this->arguments['drupal.variables'] as $variable) {
      list($key, $value) = explode(':', $variable, 2);
      if (!$this->variable_set($key, $value)) {
        $this->set_error(array('@reason' => 'failed to set variable [' . $key . '] in checkout database'));
        break;
      }
    }
  }

  /**
   * Get the relative path to a module in the checkout directory.
   *
   * @param string $module Name of the module.
   * @param boolean $reset Reset the static list of modules.
   * @return string Relative location of file, or FALSE.
   */
  protected function module_path($module, $reset = FALSE) {
    static $modules = array();

    if (!$modules || $reset) {
      // Ensure file scan directory exists
      $scanpath = $this->checkout_directory . '/' . $this->module_directory;
      if (!file_check_directory($scanpath)) {
        $this->module_directory = '';
        $scanpath = $this->checkout_directory;
      }
      $files = file_scan_directory($scanpath, '\.module$');
      foreach ($files as $file) {
        $modules[basename($file->basename, '.module')] = ltrim(str_replace($this->checkout_directory, '', dirname($file->filename)), '/');
      }
    }
    if (isset($modules[$module])) {
      return strval($modules[$module]);
    }

    $this->log('No such module [' . check_plain($module) . '].');
    $this->set_error(array('@reason' => 'No such module [' . check_plain($module) . ']'));
    return FALSE;
  }

  /**
   * Set Drupal variable in {variables} table.
   *
   * @param string $name Name of variable.
   * @param string $value Value of variable.
   * @return boolean TRUE if successfull, otherwise FALSE.
   */
  protected function variable_set($name, $value) {
    $output = "";
    $cwd = getcwd();
    $rv = $this->exec("cd {$this->checkout_directory} && drush vset {$name} '{$value}'", TRUE, $output, TRUE);
    $this->exec("cd {$cwd}");
    return $rv;
  }
}
