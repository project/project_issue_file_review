<?php

/**
 * @file
 * Provide Drupal server review implementation.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

module_load_include('server.inc', 'pifr_assertion');

/**
 * Drupal server review implementation.
 */
abstract class pifr_server_review_pifr_drupal extends pifr_server_review_pifr_assertion {

  /**
   * Modify step information and append default arguments.
   */
  public function __construct() {
    parent::__construct();

    $this->argument_default['test.extensions'] = array('php', 'inc', 'install', 'module', 'test');
    $this->argument_default += array(
      'drupal.core.version' => 7,
      'drupal.core.url' => variable_get('pifr_drupal_repository', 'git://git.drupal.org/project/drupal.git'),
      'drupal.modules' => array(),
      'drupal.user' => 'admin',
      'drupal.pass' => NULL,
      'drupal.variables' => array(),
    );

    $this->steps['syntax'] = array_merge($this->steps['syntax'], array(
      'title' => 'invalid PHP syntax',
      'active title' => 'detect invalid PHP syntax',
      'description' => 'Check the syntax of your PHP files.',
      'summary' => 'Invalid PHP syntax in @filename',
      'confirmation' => 'pifr_drupal',
    ));
    $this->steps['install'] = array_merge($this->steps['install'], array(
      'title' => 'Drupal installation failure',
      'active title' => 'detect a Drupal installation failure',
      'description' => 'Ensure that you can perform a fresh Drupal install.',
      'summary' => 'Drupal installation failed',
      'confirmation' => 'pifr_drupal',
    ));

    $this->confirmation_default['vcs']['main'] = array(
      'repository' => array(
        'type' => 'git',
        'url' => variable_get('pifr_drupal_repository', 'git://git.drupal.org/project/drupal.git'),
      ),
      'vcs_identifier' => variable_get('pifr_confirmation_vcs_id', '7.2'),
    );
  }
}
