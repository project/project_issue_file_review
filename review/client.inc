<?php
/**
 * @file
 * Provide base client review class.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * Base client review class.
 */
abstract class pifr_client_review {

  /**
   * Instance of the class.
   *
   * @var class
   */
  public static $instance;

  /**
   * Log file to which review data will be logged.
   *
   * @var string
   */
  private $log_file;

  /**
   * Directory in which project code (code under test)should be placed.
   *
   * @var string
   */
  protected $checkout_directory;

  /**
   * Directory in which patches or other material to be reviewed should go.
   *
   * @var string
   */
  protected $review_directory;

  /**
   * Original environment information.
   *
   * Keys: 'directory', 'db_prefix', 'base_url', 'base_path'.
   * $original['directory'] is the cwd when object was created.
   *
   * @var array
   */
  protected $original;

  /**
   * The operations which make up the review.
   *
   * @var array
   */
  protected $operations = array();

  /**
   * Associative array of result codes to use for pass and fail.
   *
   * @var array
   */
  protected $result_codes = array();

  /**
   * Test information, see pifr_server_xmlrpc_next().
   *
   * @var array
   * @see pifr_server_xmlrpc_next()
   */
  protected $test;

  /**
   * Associative array of arguments provided with the test.
   *
   * @var array
   */
  protected $arguments;

  /**
   * Database backend.
   *
   * @var object.
   */
  protected $database;

  /**
   * Version control backends.
   *
   * @var array
   */
  protected $vcs = array();

  /**
   * List of local files to be reviewed.
   *
   * @var array
   */
  protected $files = array();

  /**
   * Error information with keys: 'code' and 'details'.
   *
   * @var array
   */
  protected $error = array(
    'code' => 0,
    'details' => array(),
  );

  /**
   * Output of last command executed via exec().
   *
   * @var array
   */
  protected $output = array();

  /**
   * Initialize the review environment.
   */
  public function __construct(array $test) {
    global $db_prefix, $base_url, $base_path;

    // Ensure that stutus() is kept up-to-date.
    variable_set('pifr_client_operation', 'init');

    // Initialize the log file.
    $this->log_file = file_directory_path() . "/review.test_" . $test['test_id'] . ".";
    if (isset($test['review']['plugin'])) {
      $this->log_file .= $test['review']['plugin'] . '.';
    }
    if (isset($test['files'][0])) {
      $this->log_file .= basename($test['files'][0]) . '.';
    }
    $this->log_file .= time() . ".log";

    touch($this->log_file);
    // Replace with the full path now that the file is created.
    $this->log_file = realpath($this->log_file);

    // Create a symlink to the logfile at review.log
    $log_file_link = file_directory_path() . "/review.log";
    unlink($log_file_link);
    symlink(realpath($this->log_file), $log_file_link);

    $this->log("Log initialized ({$this->log_file})");
    $this->log("Review timestamp: (" . format_date(time(), 'medium') . ")");
    $testbotid = explode('.', gethostname());
    $this->log("Review host: (" . $testbotid[0] . ")");

    // Get list of operations from server implementation and remove the last
    // two operations since they should be 'fail', and 'pass'.
    require_once drupal_get_path('module', 'pifr_server') . '/pifr_server.review.inc';
    $plugin = pifr_server_review_plugin_load($test['review']['plugin']);
    $plugin->init();

    $pluginops = $plugin->operation_get_all();
    $this->operations = array_splice($pluginops, 0, -2);
    $this->result_codes = array(
      'fail' => count($this->operations) + 1,
      'pass' => count($this->operations) + 2,
    );
    if (PIFR_DEBUG) {
    	$this->log('Operations:' . print_r($this->operations, TRUE));
    	$this->log('Result Codes: ' . print_r($this->result_codes, TRUE));
    }

    // Initialize environment variables and review information.
    $this->checkout_directory = file_directory_path() . '/checkout';
    $this->review_directory = file_directory_path() . '/review';
    $this->original = array(
      'directory' => getcwd(),
      'db_prefix' => $db_prefix,
      'base_url' => $base_url,
      'base_path' => $base_path,
    );
    $this->test = $test;
    $this->arguments = $this->test['review']['argument'];
    $this->log('Environment variables and review information initialized.');

    self::$instance = $this;
  }

  /**
   * Get the operations associated with the review.
   *
   * @return array Associated operations.
   */
  public function operations_get() {
    return $this->operations;
  }

  /**
   * Log to the designated review log file.
   *
   * @param string $string String to log.
   */
  protected function log($string) {
    $string = '[' . format_date(time(),'custom', 'H:i:s') . '] ' . $string . "\n";
    if (!file_exists($this->log_file)) {
    	touch($this->log_file);
    }
    file_put_contents($this->log_file, $string, FILE_APPEND);
  }

  /**
   * Load database backend.
   *
   * @param string $type Database backend type (ie. mysql, sqlite).
   * @return boolean TRUE on success, otherwise FALSE.
   */
  protected function load_db($type) {
    $base = drupal_get_path('module', 'pifr_client') . '/review/db';
    if (file_exists("$base/$type.inc")) {
      require_once "$base/db.inc";
      require_once "$base/$type.inc";

      $class = 'pifr_client_db_interface_' . $type;
      $this->database = new $class;

      $this->log("Database backend [$type] loaded.");
      return TRUE;
    }
    $this->log("Failed to load database backend [$type].");
    return FALSE;
  }

  /**
   * Load version control system backend
   *
   * @param string $type Version control system type (ie. cvs, git).
   * @return boolean TRUE on success, otherwise FALSE.
   */
  protected function load_vcs($type) {
    if (isset($this->vcs[$type])) {
      return TRUE;
    }

    $base = drupal_get_path('module', 'pifr_client') . '/review/vcs';
    if (file_exists("$base/$type.inc")) {
      require_once "$base/vcs.inc";
      require_once "$base/$type.inc";

      $class = 'pifr_client_vcs_' . $type;
      $this->vcs[$type] = new $class;

      $this->log("Version control system backend [$type] loaded.");
      return TRUE;
    }
    $this->log("Failed to load version control system backend [$type].");
    return FALSE;
  }

  /**
   * Check if the review found an error.
   *
   * @return boolean TRUE if an error was found, otherwise FALSE.
   */
  public function has_error() {
    return $this->error['code'] != 0 || $this->error['details'];
  }

  /**
   * Get the error details.
   *
   * @return array Array of error details with keys: 'code', and 'details'.
   */
  public function get_error() {
    return $this->error;
  }

  /**
   * Set the review error.
   *
   * @param mixed $error If called from review method then an array of details
   *   about the error should be passed. When called internally the integer
   *   error code should be passed.
   */
  protected function set_error($error = array('empty')) {
    if (is_int($error)) {
      $this->error['code'] = $error;

      // Ensure that has_error() always works by setting the details to a non
      // empty array even if there are none. Once the error code has been set
      // the details should be set back to an array.
      if ($this->error['details'] == array('empty')) {
        $this->error['details'] = array();
      }
    }
    else {
      $this->error['details'] = $error;
    }
  }

  /**
   * Run the review process.
   */
  public function run() {
    $this->log('Review started.');
    if (PIFR_DEBUG) {
      $this->log('Test: ' . print_r($this->test, TRUE));
      $this->log('Operations: ' . print_r($this->operations, TRUE));
    }

    // Cycle through all operations until an error is encountered, or all the
    // operations have been completed.
    $code = 1;
    foreach ($this->operations as $operation) {
      variable_set('pifr_client_operation', $operation);
      $this->log('Invoking operation [' . $operation . ']...');
      $this->$operation();

      // Check for an error encountered during the operation.
      if ($this->has_error()) {
        // Set the code for the error in relation to the current operation.
        $this->set_error($code);

        $error = $this->get_error();
        $this->log('Encountered error on [' . $operation . "], details:\n" . var_export($error['details'], TRUE));
        break;
      }

      $code++;
    }

    $result = $this->get_result();
    $this->log("Review complete. test_id=" . $result['test_id'] . ' result code=' . $result['code'] . ' details=' . print_r($result['details'], TRUE));

    // In case any operation failed reset data members.
    global $db_prefix, $base_url, $base_path;

    chdir($this->original['directory']);
    $db_prefix = $this->original['db_prefix'];
    $base_url = $this->original['base_url'];
    $base_path = $this->original['base_path'];

    $this->log('Static variables reset.');

  }

  /**
   * Setup the review environment and ensure that everything has been cleaned.
   */
  protected function setup() {
    // Clean up checkout directory.
    if (file_exists($this->checkout_directory)) {

      // Set checkout directory permissions to allow delete
      $this->exec_output('chmod -R u+rw ' . $this->checkout_directory);

      // Remove checkout directory.
      $removed = FALSE;
      $this->exec_output('rm -rf ' . $this->checkout_directory, TRUE, $removed);
      if (!$removed) {
        $this->set_error(array('@reason' => 'failed to clear checkout directory'));
        // If unable to clear the checkout directory, this condition will
        // persist for all future tests until manually corrected. Therefore, we
        // disable the testbot so that no further tests are attempted until the
        // situation has been addressed.
        variable_set('pifr_active', !PIFR_ACTIVE);
        return;
      }

      // Clear /tmp directory
      $removed = FALSE;
      $this->exec_output('rm -rf /tmp/po*', TRUE, $removed);
      if (!$removed) {
        $this->set_error(array('@reason' => 'failed to clear checkout directory'));
      }
    }

    // Cleanup review directory.
    if (file_exists($this->review_directory)) {
      // Remove review directory.
      if (!$this->exec('rm -rf ' . $this->review_directory)) {
        $this->set_error(array('@reason' => 'failed to clear review directory'));
        return;
      }
    }

    // Create checkout directory.
    if (!file_check_directory($this->checkout_directory, FILE_CREATE_DIRECTORY)) {
      $this->set_error(array('@reason' => 'failed to create checkout directory'));
      return;
    }

    // Create review directory.
    if (!file_check_directory($this->review_directory, FILE_CREATE_DIRECTORY)) {
      $this->set_error(array('@reason' => 'failed to create review directory'));
      return;
    }

    // Ensure we can run site by using symbolic link.
    if (!file_exists('./checkout')) {
      $this->set_error(array('@reason' => 'failed to find symbolic link needed to invoke checkout'));
      return;
    }

    // Flush opcode cache to ensure no cross-contamination.
    if (function_exists('apc_clear_cache')) {
      apc_clear_cache();
      apc_clear_cache('user');
    }

    // Ensure that checkout database exists.
    global $db_url, $db_prefix;

    // Escape db prefix.
    $db_prefix = '';

    if (!is_array($db_url) || !array_key_exists('pifr_checkout', $db_url)) {
      $this->set_error(array('@reason' => 'checkout database information not found'));
      return;
    }

    // Initialize database backend.
    if (!$this->load_db(pifr_client_review_database_type())) {
      $this->error(array('@reason' => 'failed to initialize database backend'));
      return;
    }

    // Clear database.
    if (!$this->database->drop_database()) {
      $this->set_error(array('@reason' => 'failed to drop checkout database'));
      return;
    }

    // Create database.
    if (!$this->database->create_database()) {
      $this->set_error(array('@reason' => 'failed to create checkout database'));
      return;
    }
  }

  /**
   * Fetch files that are to be reviewed.
   */
  protected function fetch() {
    if (empty($this->test['files'])) {
      return; // No file(s) specified, ignore this step.
    }

    foreach ($this->test['files'] as $file) {
      // Request file from project server.
      $response = drupal_http_request($file);
      if ($response->code != 200) {
        $this->log('Failed to fetch file [' . $file . '], instead received response [' . $response->code . '].');
        $this->set_error(array('@reason' => 'failed to retrieve [' . basename($file) . '] from [' . parse_url($file, PHP_URL_HOST) . ']' ));
        return;
      }

      // Save the file data.
      $local = $this->review_directory . '/' . basename($file);
      if (file_put_contents($local, $response->data) === FALSE) {
        $this->set_error(array('@reason' => 'failed to save [' . basename($file) . '] file data'));
        return;
      }

      $this->files[] = $local;
      $this->log('Fetched [' . basename($file) . '].');
    }
  }

  /**
   * Checkout all code the be reviewed.
   */
  protected function checkout() {
    // Checkout the main branch.
    if (!empty($this->test['vcs']['main'])) {
      $this->checkout_branch($this->test['vcs']['main'], dirname($this->checkout_directory), basename($this->checkout_directory));

      $display = $this->checkout_display($this->test['vcs']['main']);
      $status = $this->has_error() ? 'failed' : 'complete';
      $this->log('Main branch [' . $display . '] checkout [' . $status . '].');
    }

    // If there are dependencies and the dependency directory has been set then
    // check them out into the dependency directory.
    if (!empty($this->test['vcs']['dependencies']) && !empty($this->arguments['test.directory.dependency'])) {
      // Ensure that dependency directory exists.
      $directory = $this->checkout_directory . '/' . $this->arguments['test.directory.dependency'];
      if (!file_check_directory($directory, FILE_CREATE_DIRECTORY)) {
        $this->set_error(array('@reason' => 'failed to create dependency directory'));
        return;
      }

      // Checkout dependencies into the dependency directory.
      foreach ($this->test['vcs']['dependencies'] as $dependency) {
        if ($this->has_error()) break;

        $this->checkout_branch($dependency, $directory);

        $display = $this->checkout_display($dependency);
        $status = $this->has_error() ? 'failed' : 'complete';
        $this->log('Dependency branch [' . $display . '] checkout [' . $status . '].');
      }
    }
  }

  /**
   * Checkout a branch.
   *
   * @param array $info Branch information, pulled form test information.
   * @param string $directory Root directory to checkout into.
   * @param string $vcs_directory (optional) Directory that the checkout should
   *   be placed in, otherwise the module name will be used.
   */
  protected function checkout_branch($info, $directory, $vcs_directory = FALSE) {
    if (!file_check_directory($this->checkout_directory, FILE_CREATE_DIRECTORY)) {
      $this->set_error(array('@reason' => 'failed to create checkout directory'));
      return;
    }

    $type = $info['repository']['type'];
    if (!$this->load_vcs($type)) {
      $this->set_error(array('@reason' => 'failed to initialize VCS backend'));
      return;
    }

    // Change the directory to one level outside the directory to checkout into.
    chdir($directory);

    // Attempt to checkout the branch.
    if (!$this->vcs[$type]->checkout($vcs_directory, $info['repository']['url'], $info['vcs_identifier'])) {
      $this->set_error(array('@reason' => 'failed to checkout from [' . $info['repository']['url'] . ']'));
      return;
    }

    // Return to the original directory.
    chdir($this->original['directory']);
  }

  /**
   * Get a display value for the vcs information.
   *
   * @return string Display value.
   */
  protected function checkout_display($info) {
    return $this->vcs[$info['repository']['type']]->display($info['repository']['url'], $info['vcs_identifier']);
  }

  /**
   * Check file(s) for non-unix line endings.
   */
  protected function check() {
    if (empty($this->test['files'])) {
      // No file(s) specified, ignore this step.
      $this->log('Ignoring operation [check].');
      return;
    }

    foreach ($this->files as $file) {
      $contents = file_get_contents($file);
      if (preg_match('/^\+ (.*)\\r$/m', $contents)) {
        $this->set_error(array('@filename' => basename($file)));
        break;
      }
    }
  }

  /**
   * Apply any patch(s).
   *
   * @param string $directory (Optional) The base directory to apply patches.
   * @param array $files (Optional) List patches to apply.
   */
  protected function apply($directory = NULL, array $files = array()) {
    // Assume defaults if no custom values set.
    if (!$directory) {
      $directory = $this->checkout_directory . '/' . $this->arguments['test.directory.apply'];
    }
    if (!$files) {
      $files = $this->apply_files();
    }

    if (empty($files)) {
      // No patch(s) specified, ignore this step.
      $this->log('Ignoring operation [apply].');
      return;
    }

    foreach ($files as $file) {
      // Use realpath() to ensure file path works after chdir().
      $file = realpath($file);

      chdir($directory);

      if (!$this->vcs[$this->test['vcs']['main']['repository']['type']]->apply($file)) {
        $error = array('@filename' => basename($file), '@reason' => 'Unable to apply patch.  See the log in the details link for more information');
        $this->set_error($error);
        return;
      }

      // Generating log messages listing the files the patch changed.
      $changed = $this->vcs[$this->test['vcs']['main']['repository']['type']]->changed_files($file);
      $changed = $changed ? implode("\n > ", $changed) : 'no files';
      $this->log('Applied [' . basename($file) . "] to:\n > $changed");

      chdir($this->original['directory']);
    }
  }

  /**
   * Get the files that can be applied.
   *
   * @return array List of files.
   */
  protected function apply_files() {
    $files = array();
    foreach ($this->files as $file) {
      if (preg_match('/(\.diff|\.patch)$/m', $file)) {
        $files[] = $file;
      }
    }
    return $files;
  }

  /**
   * Check the syntax of the checkout after any patching.
   */
  protected function syntax() {
    if (empty($this->test['vcs']['main']) || !empty($this->test['omit_syntax_check'])) {
      // No main repository.
      $this->log('Ignoring operation [syntax].');
      return;
    }

    // Get the relevant files to check for syntax.
    $files = $this->syntax_files();

    // Remove files to be ignored.
    $files = $this->syntax_ignore($files);

    // All files are relatvie to the checkout directory.
    chdir($this->checkout_directory);

    $not_found = 0;
    foreach ($files as $file) {
      // Ensure that the file exists and is not a reference to a file that was
      // removed in a patch, or the like.
      if (!file_exists($file)) {
        $not_found++;
        continue;
      }

      // Check the syntax of the file using the abstracted method.
      if (!$this->syntax_check($file)) {
        $this->set_error(array('@filename' => $file));
        return;
      }
    }

    // Return to original directory.
    chdir($this->original['directory']);

    $this->log('Checked [' . (count($files) - $not_found) . ' of ' . count($files) . '] relevant file(s) for syntax.');
  }

  /**
   * Get the relevant files to check for syntax.
   *
   * @return array List of files relative to the checkout directory.
   */
  protected function syntax_files() {
    $files = array();
    $applied_files = $this->apply_files();
    if (!empty($applied_files)) {
      // Get a list of all the files to which code has been added or removed.
      $type = $this->test['vcs']['main']['repository']['type'];
      foreach ($applied_files as $file) {
        $files = array_merge($files, $this->vcs[$type]->changed_files($file));
      }

      // If the files were applied from a directory other than the root correct
      // the file paths returned from the VCS backend.
      if (!empty($this->arguments['test.directory.apply'])) {
        foreach ($files as &$file) {
          $file = $this->arguments['test.directory.apply'] . '/' . $file;
        }
      }
    }
    elseif (!empty($this->arguments['test.files'])) {
      foreach ($this->arguments['test.files'] as $file) {
        if (file_exists($this->checkout_directory . '/' . $file)) {
          $files[] = $file;
        }
      }
    }
    else {
      foreach ($this->arguments['test.directory.review'] as $key => $directory) {
        // Strip prefix directories off for coder plugin
        // Coder checks out it's code directoy to /checkout/
        if ($this->test['review']['plugin'] == 'pifr_coder') {
          $directory = '';
        }
        // Check all the files in the review directory and make their paths
        // relative to the checkout directory.
        $base = rtrim($this->checkout_directory . '/' . $directory, '/');
        $scan = file_scan_directory($base, '.*');
        foreach ($scan as $file) {
          $files[] = str_replace($this->checkout_directory . '/', '', $file->filename);
        }
      }
    }
    return $files;
  }

  /**
   * Filter out files to ignore.
   *
   * @param array $files List of files to be syntax checked.
   * @return array Filtered list of files to be syntax checked.
   */
  protected function syntax_ignore(array $files) {
    // If extensions are specified then filter the files.
    if ($extensions = $this->arguments['test.extensions']) {
      foreach ($files as $key => $file) {
        if (!in_array(pathinfo($file, PATHINFO_EXTENSION), $extensions)) {
          unset($files[$key]);
        }
      }
    }
    return $files;
  }

  /**
   * Check the syntax of a file.
   *
   * @param string Path to file relative to checkout directory.
   * @return boolean TRUE if file passed syntax check, otherwise FALSE.
   */
  protected abstract function syntax_check($file);

  /**
   * Get the review results.
   *
   * @return array Test results information, see pifr_server_xmlrpc_result().
   * @see pifr_server_xmlrpc_result()
   */
  public function get_result() {
    $result = array(
      'test_id' => $this->test['test_id'],
      'code' => $this->error['code'] ? $this->error['code'] : count($this->operations) + 1,
      'details' => $this->error['details'],
      'data' => array(),
      'log' => trim(file_get_contents($this->log_file)),
    );

    return $result;
  }

  /**
   * Get the current status of the review.
   *
   * @return string Summary of current status.
   */
  public static function status() {
    return t('Currently on operation [@operation].', array('@operation' => variable_get('pifr_client_operation', 'init')));
  }

  /**
   * Wrapper for PHP exec() function.
   *
   * @param string $command Command to execute.
   * @param boolean $capture_stderr Capture the stderr by mapping it to the
   *   stdout and collecting it on failure in the review log.
   * @param $returned_output
   * @param $log_success_output If true, logs the actual command output upon
   *   successful execution.  Default is to log 'command succeeded'.
   * @return boolean TRUE if success, otherwise FALSE.
   */
  public static function exec($command, $capture_stderr = TRUE, &$returned_output = NULL, $log_success_output = FALSE) {
    if ($capture_stderr) {
      $command .= ' 2>&1';
    }
    $cwd = getcwd();
    $now = time();

    exec($command, $output, $status);
    $duration = time() - $now;

    $output = trim(implode("\n", $output));

    self::$instance->output = $returned_output = $output;
    if ($status != 0) {
      self::$instance->log("Command [$command] failed\n  Duration $duration seconds\n  Directory [$cwd]\n  Status [$status]\n Output: " . ($output ? "[$output]" : 'no output') . '.');
      return FALSE;
    }
    if ($log_success_output) {
      self::$instance->log("Command [$command] succeeded\n  Duration: $duration seconds\n  Directory: [$cwd]\n  Completion status: [$status]\n  Output: " . ($output ? "[$output]" : 'no output') . '.');
    }
    else {
      self::$instance->log("Command [$command] succeeded.");
    }
    return TRUE;
  }

  /**
   * Wrapper for PHP exec() function.
   *
   * @param string $command Command to execute.
   * @param boolean $capture_stderr Capture the stderr by mapping it to the
   *   stdout and collecting it on failure in the review log.
   * @param boolean $shell_return_value TRUE if success, FALSE if failure.
   * @return string Command output.
   */
  public static function exec_output($command, $capture_stderr = TRUE, &$shell_return_value = TRUE) {
    $shell_return_value = self::exec($command, $capture_stderr);
    return self::$instance->output;
  }
}
