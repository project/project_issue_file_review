<?php

/**
 * @file
 * Provide SimpleTest server review implementation.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

module_load_include('server.inc', 'pifr_drupal');

/**
 * SimpleTest server review implementation.
 */
class pifr_server_review_pifr_simpletest extends pifr_server_review_pifr_drupal {

  /**
   * Modify step information and append default arguments.
   */
  public function __construct() {
    parent::__construct();

    $this->argument_default += array(
      'simpletest.tests' => array(),
      'simpletest.db' => NULL,
    );

    $this->steps['review'] = array_merge($this->steps['review'], array(
      'title' => 'test run failure',
      'active title' => 'detect a test run failure',
      'description' => 'Ensure that you can install SimpleTest locally and invoke tests using the command line script.',
      'summary' => 'Failed to run tests: @reason',
      'confirmation' => 'pifr_simpletest',
    ));
    $this->steps['fail'] = array_merge($this->steps['fail'], array(
      'title' => 'detect a failing test',
      'active title' => 'detect a failing test',
      'description' => 'Review the test failures below then if necessary run them locally until the problem has been fixed.',
      'confirmation' => 'pifr_simpletest',
    ));
    $this->steps['pass'] = array_merge($this->steps['pass'], array(
      'title' => 'all tests pass',
      'active title' => 'complete test suite with all tests passing',
      'confirmation' => 'pifr_simpletest',
    ));
  }

  /**
   * Add the 'tests' argument when performing the fail operation.
   */
  protected function confirmation_test_change($key, $module) {
    $changes = parent::confirmation_test_change($key, $module);

    // Only run the NonDefaultBlockAdmin to detect pass/fail test
    // results. This will cut down the time required for confirmation.
    if (($key == 'fail') || PIFR_SHORTCUT_CONFIRM_TEST) {
      $changes['review'] = array(
        'argument' => array(
          'simpletest.tests' => array('NonDefaultBlockAdmin'),
        )
      );
    }

    return $changes;
  }
}
