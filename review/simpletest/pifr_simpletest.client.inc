<?php

/**
 * @file
 * Provide SimpleTest client review implementation.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

module_load_include('client.inc', 'pifr_drupal');

/**
 * SimpleTest PIFR review implementation.
 */
class pifr_client_review_pifr_simpletest extends pifr_client_review_pifr_drupal {

  protected $skipped = FALSE;

  /**
   * Add clone-db URL to list of files to be downloaded (if set).
   */
  public function __construct(array $test) {
    parent::__construct($test);

    // If an import database is provided then download the database export.
    if (!empty($this->arguments['simpletest.db'])) {
      $this->test['files'][] = $this->arguments['simpletest.db'];
    }

    if ($this->arguments['drupal.core.version'] < 8) {
      // Set SimpleTest variables to ensure that results are left in database
      // and that verbose mode is disabled.
      $this->arguments['drupal.variables'][] = 'simpletest_clear_results:0';
      $this->arguments['drupal.variables'][] = 'simpletest_verbose:0';
    }
  }

  /**
   * Import clone database (if set) before performing Drupal installation.
   */
  protected function install() {
    parent::install();

    if (!$this->has_error() && !empty($this->arguments['simpletest.db'])) {
      // Drupal install() will have taken care of settings.php and such so drop
      // and create database to be filled by clone-db.
      $this->database->drop_database();
      $this->database->create_database();

      // Since a database import is being used the username and password must
      // be included in arguments.
      if (empty($this->arguments['drupal.user']) || empty($this->arguments['drupal.pass'])) {
        $this->set_error(array('@reason' => 'both [drupal.user] and [drupal.pass] must be specified'));
        return;
      }

      $basename = basename($this->arguments['simpletest.db']);
      foreach ($this->files as $file) {
        if (basename($file) == $basename) {
          $this->log('Importing clone database from [' . filter_xss($basename) . ']...');

          if ($result = $this->database->import($file, 'clone_db')) {
            $this->log('Imported [' . $result . '] tables.');
          }
          else {
            $this->set_error(array('@reason' => 'failed to import clone database'));
          }
          break;
        }
      }

      // Reset variables since database was just rewritten.
      $this->install_variables($this->arguments['drupal.variables']);
    }
  }

  /**
   * Run SimpleTest tests.
   */
  protected function review() {
    global $base_url, $base_path, $db_url;

    // If Drupal 6 then we need to perform SimpleTest installation.
    if ($this->arguments['drupal.core.version'] == 6) {
      if (!$this->drupal6_test_prepare()) {
        return;
      }
    }
    // Drupal 8 does not need a parent/test-runner site.
    if ($this->arguments['drupal.core.version'] < 8) {
      $cwd = getcwd();
      $output = "";
      $result = $this->exec("cd {$this->checkout_directory} && drush en -y simpletest", TRUE, $output, TRUE);
      $this->exec("cd {$cwd}");
      if ($result !== TRUE) {
        $this->set_error(array('@reason' => "Failed to drush en simpletest, output={$output}" ));
        return;
      }
    }

    // Assumes it is currently outside of checkout directory.
    $test_list = $this->test_list();

    // Run tests from command line.
    chdir($this->checkout_directory);

    $run_tests = file_exists('scripts/run-tests.sh') ? 'scripts/run-tests.sh' : 'core/scripts/run-tests.sh';
    // Some tests are sensitive to a trailing /, so make sure it doesn't have one.
    $url = url(NULL, array('absolute' => TRUE));
    $url = trim($url, '/');

    // Set up 'run-tests.sh' command
    $command = PIFR_CLIENT_PHP . ' ./' . $run_tests;
    $command .= ' --concurrency ' . PIFR_CLIENT_CONCURRENCY;
    $command .= ' --php ' . PIFR_CLIENT_PHP;
    $command .= ' --url ' . $url;
    if ($this->arguments['drupal.core.version'] >= 8) {
      $command .= ' --sqlite /tmpfs/pifr/test.sqlite';
      $command .= ' --dburl ' . $db_url['pifr_checkout'];
      $command .= ' --keep-results';
    }
    $command .= ' ' . $test_list;

    // Attempt to clear out any tables from previous runs
    $this->log('Cleaning run-tests.sh environment.');
    if (!$this->exec($command . ' --clean')) {
      $this->set_error(array('@reason' => t('failed during invocation of run-tests.sh --clean')));
      return;
    }
    // Determine whether there are any simpletest tests to validate
    // If no tests found in the project (verified by $test_list returning an
    // empty "--file " parameter with no arguments), there is no need to
    // execute run_tests.sh.
    if ($test_list != "--file ") {
      $this->log('Initiating run-tests.sh.');
      // Kick off actual testing
      $var = NULL;
      if (!$this->exec($command, TRUE, $var, TRUE)) {
        $this->set_error(array('@reason' => t('failed during invocation of run-tests.sh')));
        return;
      }
      // Ensure no PHP Fatal errors in the output, which can result in a
      // '0 tests passed' false positive
      if (strpos($this->output, "PHP Fatal error") !== FALSE) {
        $this->set_error(array('@reason' => t('PHP Fatal error encountered during run_tests.sh.  See review log for details.')));
        return;
      }
      // Since a blank file argument was not passed then an error was
      // encountered since we were expecting tests. The valid case for this
      // message being printed would be calling with "--file ".
      if (strpos($this->output, 'ERROR: No valid tests were specified.') !== FALSE) {
        $this->set_error(array('@reason' => t('run-tests.sh reported no tests were found.  See review log for details.')));
        return;
      }
    }
    else {
      // Note in the review log that we didn't execute any tests.
      $this->log('No test files found.  Skipping execution of run_tests.sh.');
      $this->skipped = TRUE;
    }
    // Get test info for use by test_info_parse().
    if (!$this->exec(PIFR_CLIENT_PHP . ' ./' . $run_tests . ' --php ' . PIFR_CLIENT_PHP . ' --url ' . $url . ' ' . $test_list . ' --list')) {
      $this->set_error(array('@reason' => t('failed attempting to get list of tests from run-tests.sh')));
      return;
    }

    chdir($this->original['directory']);
  }

  /**
   * Perform Drupal 6 SimpleTest installation.
   */
  protected function drupal6_test_prepare() {
    // Apply Simpletest core patch.
    $file = getcwd() . '/' . drupal_get_path('module', 'pifr_simpletest') . '/D6-core-simpletest.patch';
    chdir($this->checkout_directory);
    $this->log('Applying in ' . getcwd(). ' file=' . $file);
    if (!$this->vcs[$this->test['vcs']['main']['repository']['type']]->apply($file, 1)) {
      $error = array('@filename' => basename($file));
      $this->set_error($error);
    	return FALSE;
    }

    // Move run-tests.sh to scripts directory.
    if (!copy($this->module_directory . '/simpletest/run-tests.sh', 'scripts/run-tests.sh')) {
      $this->set_error(array('@reason' => 'failed to install run-tests.sh'));
      return FALSE;
    }
    chdir($this->original['directory']);

    $this->log('Completed Drupal 6 SimpleTest installation.');

    return TRUE;
  }

  /**
   * Create the test argument for run-tests.sh.
   *
   * @return string Tests argument to be appended to run-test.sh command.
   */
  protected function test_list() {
    if (!empty($this->arguments['simpletest.tests'])) {
      return '--class "' . implode(',', $this->arguments['simpletest.tests']) . '"';
    }
    // For legacy compatibility with early tests which used drupal.modules
    elseif (!empty($this->arguments['drupal.modules'])) {
      $args = array();
      // Scan each module's directory tree for .test files
      foreach ($this->arguments['drupal.modules'] as $module) {
        $args += $this->findTestFiles($this->module_path($module));
      }
      return '--file ' . implode(',', $args);
    }
    // This is the current case for contributed projects (but not for core).
    elseif (!empty($this->arguments['test.directory.review'][0])) {
      $args = array();
      // Scan the 'test.directory.review' directory for '.test' files
      foreach ($this->arguments['test.directory.review'] as $directory) {
        $args += $this->findTestFiles($directory);
      }
      return '--file ' . implode(',', $args);
    }
    // If neither test.directory.review or drupal.modules is present, this
    // prevents contrib tests from running through a complete core test suite.
    elseif (!($this->is_core_test())) {
      $args = array();
      // Not a core test?  Scan the sites/all/modules/modulename directory
      $this->log("Not a core test.  Scanning module directory.");
      $args += $this->findTestFiles($this->module_directory . '/' . $this->project_directory);
      return '--file ' . implode(',', $args);
    }
    // Short-circuit for the special PIFR self-test helper command.
    // @todo Move this to the top, and make it core major version dependent.
    elseif (PIFR_SHORTCUT_CORE_TEST) {
      return '--class NonDefaultBlockAdmin';
    }
    // If none of the above conditions were met, then this is a Drupal core test,
    // so all available tests need to be run.
    return '--all';
  }

  /**
   * Finds files containing tests.
   *
   * File locations and extensions vary on core major version.
   *
   * @param string $directory
   *   A directory to scan within $this->checkout_directory.
   *
   * @return array
   *   A list of filenames that contain tests. Filepaths are relative to
   *   $this->checkout_directory.
   *
   * @see test_list()
   */
  protected function findTestFiles($directory) {
    $basedir = $this->checkout_directory . '/';
    $files = array();

    // D7-: Find all .test files, which can be located anywhere within $directory.
    $this->log('Scanning ' . $basedir . $directory . ' for .test files.');
    foreach (file_scan_directory($basedir . $directory, '\.test$') as $file) {
      $filename = str_replace($basedir, '', $file->filename);
      $files[$filename] = $filename;
    }
    // D8+: Find all tests in the PSR-X structure; Drupal\$extension\Tests\*.php
    // Since we do not want to hard-code too many structural file/directory
    // assumptions about PSR-0/4 files and directories, we check for the minimal
    // conditions only; i.e., a '*.php' file that has '/Tests/' in its path.
    $this->log('Scanning ' . $basedir . $directory . ' for /Tests/*.php files.');
    // Ignore anything from third party vendors.
    $ignore = array('.', '..', 'vendor');
    foreach (file_scan_directory($basedir . $directory, '\.php$', $ignore) as $file) {
      // '/Tests/' can be contained anywhere in the file's path (there can be
      // sub-directories below /Tests), but must be contained literally.
      // Case-insensitive to match all Simpletest and PHPUnit tests:
      //   ./lib/Drupal/foo/Tests/Bar/Baz.php
      //   ./foo/src/Tests/Bar/Baz.php
      //   ./foo/tests/Drupal/foo/Tests/FooTest.php
      //   ./foo/tests/src/FooTest.php
      if (stripos($file->filename, '/Tests/')) {
        $filename = str_replace($basedir, '', $file->filename);
        $files[$filename] = $filename;
      }
    }

    return $files;
  }

  /**
   * Attempt to identify whether a test is core
   */
  public function is_core_test() {
    // If the project in vcs[main][repository][url] is not the drupal core url,
    // then this is not a core test.
    if ($this->test['vcs']['main']['repository']['url'] != $this->arguments['drupal.core.url']) {
      return FALSE;
    }
    // If above IS drupal, we need to check dependencies
    if (!empty($this->test['vcs']['dependencies'])) {
      foreach ($this->test['vcs']['dependencies'] as $module) {
        // To obtain the dependency project name, we seperate the project from
        // the path component, and then strip off the vcs file extension from
        // the file name.
        $repo_url = $module['repository']['url'];
        $extension = '.' . $module['repository']['type'];
        $project = str_replace($extension, '', array_pop(explode('/', $repo_url)));
        switch ($this->arguments['drupal.core.version']) {
          case '8':
          case '7':
            // D7/8 core has no required dependencies. If one exists, it's a
            // module test
            if ($project != 'drupal') {
              return FALSE;
            }
            break;
          case '6':
            // D6 core has a dependency on simpletest.
            if (!in_array($project, array('drupal', 'simpletest'))) {
              return FALSE;
            }
        }
      }
    }
    // If we get to this point, assume a core test
    return TRUE;
  }

  /**
   * Add the SimpleTest specific results.
   */
  public function get_result() {
    $result = parent::get_result();

    if ($this->arguments['drupal.core.version'] < 8) {
      $database = $this->database;
    }
    else {
      // Testbots change the working directory. Build a relative path based on
      // the location of this file.
      require_once __DIR__ . '/../../client/review/db/db.inc';
      require_once __DIR__ . '/../../client/review/db/sqlite_d8runner.php';
      $database = new pifr_client_db_interface_sqlite_d8runner();
    }
    // If result code is fail then all the operations passed, so the test
    // results need to be added and the proper result code assigned.
    if ($result['code'] == $this->result_codes['fail']) {
      $result['details'] = array(
        '@pass' => 0,
        '@fail' => 0,
        '@exception' => 0,
        '@debug' => 0,
      );
      $result['data'] = array();

      // Cycle through assertions to fill in results.
      $info = $this->test_info_parse();

      // Process assertions in batches of 10,000 to keep down memory usage.
      $message_id = 0;
      do {
        $assertions = $database->query('SELECT *
                                        FROM {simpletest}
                                        WHERE message_id > ' . $message_id . '
                                        ORDER BY message_id
                                        LIMIT 10000');
        $non_pass = 0;
        foreach ($assertions as $assertion) {
          // Fill in details with summary of assertion counts.
          $result['details']['@' . $assertion['status']]++;

          // Initialize each test class.
          if (!isset($result['data'][$assertion['test_class']])) {
            $result['data'][$assertion['test_class']] = array(
              'test_name' => !empty($info[$assertion['test_class']]) ? $info[$assertion['test_class']] : $assertion['test_class'],
              'pass' => 0,
              'fail' => 0,
              'exception' => 0,
              'debug' => 0,
              'assertions' => array(),
            );
          }

          // Add up summary totals for test class.
          $result['data'][$assertion['test_class']][$assertion['status']]++;

          // Store non-pass assertion until reaching maximum number.
          if ($assertion['status'] != 'pass' && $non_pass < PIFR_CLIENT_MAX_ASSERTIONS) {
            // Remove non-standard keys.
            unset($assertion['message_id']);
            unset($assertion['test_id']);

            // Make file only include name, instead of absolute reference.
            $assertion['file'] = basename($assertion['file']);

            // Add assertion to set of non-pass assertions for the class.
            $result['data'][$assertion['test_class']]['assertions'][] = $assertion;

            // Keep a record of the number of non-pass assertions.
            $non_pass++;
          }
        }

        $message_id += 10000;
      }
      while ($assertions);

      // Remove class name keys.
      $result['data'] = array_values($result['data']);

      // If no assertions were retreived from the database then set result to
      // error since there should be at least one assertion. By this point the
      // code in review() should have checked for any error or valid case that
      // would return no results and either already exited or set $this->skipped
      // to TRUE.
      if (!$this->skipped && array_sum($result['details']) == 0) {
        $result['code']--; // Revert code back to review step.
        $result['details'] = array('@reason' => 'tests were executed, but no results were found');
      }
      else {
        // Assign the code based on the test results.
        $result['code'] = $this->result_codes[$result['details']['@fail'] + $result['details']['@exception'] ? 'fail' : 'pass'];
      }
    }
    return $result;
  }

  /**
   * Parse test info from 'run-tests.sh --list' output.
   *
   * @return array Associative array of test info keyed by test case.
   */
  protected function test_info_parse() {
    if (!is_array($this->output)) {
      $this->output = explode("\n", $this->output);
    }
    $this->output = array_slice($this->output, 4);
    $group = 'Unknown';
    $info = array();
    foreach ($this->output as $line) {
      if (isset($line[1]) && $line[1] == '-') {
        if (preg_match('/^ - (.*?) \((\w+)\)$/m', $line, $match)) {
          $info[$match[2]] = $match[1] . ' (' . $match[2] . ') [' . $group . ']';
        }
      }
      else {
        $group = $line;
      }
    }
    return $info;
  }

  /**
   * If on review operation then display the assertion count.
   */
  public static function status() {
    if (variable_get('pifr_client_operation', 'init') == 'review') {
      require_once drupal_get_path('module', 'pifr_client') . '/review/db/db.inc';
      // We can not detect that we're running Drupal 8 tests since this is a
      // static method that runs on bots whilst they are running tests. So just,
      // look for the sqlite file and if it exist assume Drupal 8.
      if (file_exists('/tmpfs/pifr/test.sqlite')) {
        require_once drupal_get_path('module', 'pifr_client') . '/review/db/sqlite_d8runner.php';
        $database = new pifr_client_db_interface_sqlite_d8runner();
      }
      else {
        module_load_include('review.inc', 'pifr_client');
        $type = pifr_client_review_database_type();
        require_once drupal_get_path('module', 'pifr_client') . '/review/db/' . $type . '.inc';
        $database = 'pifr_client_db_interface_' . $type;
        $database = new $database;
      }

      $count = $database->query('SELECT COUNT(*) FROM {simpletest}');
      return t('Running tests, @count assertion(s).', array('@count' => number_format($count ? $count[0]['COUNT(*)'] : 0)));
    }
    return parent::status();
  }
}
