<?php

/**
 * @file
 * Provide general assertion server review implementation.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * General assertion server review implementation.
 */
abstract class pifr_server_review_pifr_assertion extends pifr_server_review {

  /**
   * Labels to be displayed to user.
   *
   * @var array
   */
  protected $labels = array(
    'pass' => 'pass',
    'fail' => 'fail',
    'exception' => 'exception',
  );

  /**
   * Standard detail message.
   *
   * @var string
   */
  protected $detail_message = '@pass pass(es), @fail fail(s), and @exception exception(s)';

  /**
   * Modify step information.
   */
  public function __construct() {
    $this->steps['fail'] = array_merge($this->steps['fail'], array(
      'description' => 'Review the test failures below then if necessary run them locally until the problem has been fixed.',
      'summary' => $this->detail_message,
    ));
    $this->steps['pass'] = array_merge($this->steps['pass'], array(
      'summary' => '@pass pass(es)',
    ));
  }

  /**
   * Format the result detail numbers.
   *
   * Performs a number format on each of the three assertion summary counts:
   * pass, fail, exception (with @ in front of each).
   */
  public function summary_format(array $details) {
    foreach (array('@pass', '@fail', '@exception') as $key) {
      if (!empty($details[$key])) {
        $details[$key] = number_format($details[$key]);
      }
    }
    return $details;
  }

  /**
   * Generate detailed display of test results.
   *
   * The display will provide test summaries and assertions as applicable.
   *
   * @param array $test Test information.
   * @param array $result Test result information.
   * @return string HTML output.
   */
  protected function detail_display(array $test, array $result) {
    // Map of status images.
    $map = array(
      'pass' => theme('image', 'misc/watchdog-ok.png'),
      'fail' => theme('image', 'misc/watchdog-error.png'),
      'exception' => theme('image', 'misc/watchdog-warning.png'),
      'debug' => theme('image', 'misc/watchdog-warning.png'),
    );

    // Map of arrow images.
    $js = array(
      theme('image', 'misc/menu-collapsed.png', 'Expand', 'Expand', array('class' => 'pifr-assertion')),
      theme('image', 'misc/menu-expanded.png', 'Collapsed', 'Collapsed', array('class' => 'pifr-assertion')),
      theme('image', 'misc/menu-leaf.png', 'Leaf', 'Leaf', array('class' => 'pifr-assertion')),
    );
    drupal_add_js(array('pifr_assertion' => $js), 'setting');

    // Build list of test summaries.
    $header = array(t('Test name'));
    foreach ($this->labels as $label) {
      $header[] = t(ucfirst($label));
    }

    $rows = array();
    $rows_fail = array();
    foreach ($result['data'] as $detail) {
      // Display the summary of the test.
      $row = array();

      $row[] = ($detail['assertions'] ? $js[0] : $js[2]) . $detail['test_name'];
      $row[] = $detail['pass'];
      $row[] = $detail['fail'];
      $row[] = $detail['exception'];

      // Determine proper class for coloring.
      $class = 'pass';
      if ($detail['fail']) {
        $class = 'fail';
      }
      elseif ($detail['exception']) {
        $class = 'exception';
      }
      $rows[] = array(
        'class' => 'pifr-' . $class,
        'data' => $row,
      );
      if ($class != 'pass') {
        $rows_fail[] = $rows[count($rows) - 1];
      }

      // If individual assertions where included, display them below the
      // summary row with the ability to expand/collapse via javascript.
      if ($detail['assertions']) {
        $header_detail = array(t('Message'), t('Group'), t('Filename'), t('Line'), t('Function'), t('Status'));
        $rows_detail = array();
        foreach ($detail['assertions'] as $assertion) {
          $row = array();
          $row[] = filter_xss($assertion['message']);
          $row[] = check_plain($assertion['message_group']);
          $row[] = check_plain($assertion['file']);
          $row[] = $assertion['line'];
          $row[] = check_plain($assertion['function']);
          $row[] = $map[$assertion['status']];

          $rows_detail[] = array(
            'class' => 'pifr-' . $assertion['status'],
            'data' => $row
          );
        }

        // Add header for assertion messages and include assertion rows.
        $row = array();
        $row[] = array(
          'colspan' => 4,
          'data' => theme('table', $header_detail, $rows_detail),
        );
        $rows[] = array(
          'class' => 'pifr-assertion-wrapper',
          'data' => $row,
        );
        if ($class != 'pass') {
          $rows_fail[] = $rows[count($rows) - 1];
        }
      }
    }

    // Generate summary of assertion result.
    $summary = '';
    if (isset($result['details']['@fail'])) {
      $pass = $result['details']['@fail'] + $result['details']['@exception'] == 0;
      $details = $this->summary_format($result['details']);
      $summary = t($this->detail_message, $details);
      $summary = '<div class="pifr-assertion-summary pifr-' . ($pass ? 'ok' : 'error') . '">' . $summary . '</div>';
    }

    // Display failed rows above a table containing all rows.
    if ($rows_fail) {
      return $summary .
        '<h3>' . t('Non-pass') . '</h3>' . theme('table', $header, $rows_fail) .
        '<h3>' . t('All') . '</h3>' . theme('table', $header, $rows);
    }

    // Merge all components.
    return $rows ? $summary . theme('table', $header, $rows) : '';
  }

  /**
   * The display will provide a list of non-pass tests.
   */
  public function detail_summary(array $test, array $result) {
    $failed_tests = array();
    foreach ($result['data'] as $detail) {
      if ($detail['fail'] + $detail['exception'] > 0) {
        // Add non-pass assertion to summary details.
        $assertions = array();
        foreach ($detail['assertions'] as $assertion) {
          $assertions[] = t('[@status] [@group] "!message" in @file on line @line of @function.', array(
            '@status' => $this->labels[$assertion['status']],
            '@group' => $assertion['message_group'],
            '!message' => filter_xss($assertion['message']),
            '@file' => $assertion['file'],
            '@line' => $assertion['line'],
            '@function' => $assertion['function'],
          ));
        }

        // Generate summary of test group.
        $details = array(
          '@pass' => $detail['pass'],
          '@fail' => $detail['fail'],
          '@exception' => $detail['exception'],
        );
        $details = t($this->detail_message, $details);

        // Add non-pass test to list.
        $failed_tests[] = array(
          'data' => $detail['test_name'] . ' (' . $details . ')',
          'children' => $assertions
        );
      }
    }

    if (!$failed_tests) {
      $failed_tests[] = array('data' => t('No relevant data'));
    }

    // Manually render list since it contains children which need special
    // attention when converting to text.
    $out = '';
    foreach ($failed_tests as $test) {
      $out .= ' * ' . $test['data'] . "\n";
      if (!empty($test['children'])) {
        foreach ($test['children'] as $assertion) {
          $out .= '   - ' . $assertion . "\n";
        }
      }
      $out .= "\n";
    }
    return $out;
  }

  /**
   * Load tree of assertion information.
   */
  public function load($result_id) {
    $data = array();

    $details = db_query('SELECT *
                         FROM {pifr_result_detail}
                         WHERE result_id = %d
                         ORDER BY test_name', $result_id);
    while ($detail = db_fetch_array($details)) {
      $detail_assertions = db_query('SELECT *
                                     FROM {pifr_result_detail_assertion}
                                     WHERE result_detail_id = %d
                                     ORDER BY result_detail_assertion_id', $detail['result_detail_id']);
      $detail['assertions'] = array();
      while ($detail_assertion = db_fetch_array($detail_assertions)) {
        $detail['assertions'][] = $detail_assertion;
      }
      $data[] = $detail;
    }

    return $data;
  }

  /**
   * Record tree of assertion information.
   */
  public function record($result_id, array $data) {
    foreach ($data as $detail) {
      $detail['result_id'] = $result_id;
      drupal_write_record('pifr_result_detail', $detail);

      foreach ($detail['assertions'] as $assertion) {
        $assertion['result_detail_id'] = $detail['result_detail_id'];
        drupal_write_record('pifr_result_detail_assertion', $assertion);
      }
    }
  }

  /**
   * Delete tree of assertion information.
   */
  public function delete($result_id, array $data) {
    foreach ($data as $detail) {
      db_query('DELETE FROM {pifr_result_detail_assertion}
                WHERE result_detail_id = %d', $detail['result_detail_id']);
    }
    db_query('DELETE FROM {pifr_result_detail}
              WHERE result_id = %d', $result_id);
  }
}
