
/**
 * Sawp out examples and provide in-memory editting.
 */
Drupal.behaviors.pifrShellMethod = function() {
  
  var previousMethod = '';
  
  setInput($('#edit-method').val().replace('.', '-'));

  $('#edit-method').change(function() {
    // Store current input.
    $('#input-example-' + previousMethod).html($('#edit-input').val());
    
    // Set to next example, or previous input.
    setInput($(this).val().replace('.', '-'));
  });
  
  /**
   * Set the current input text based on the selected method.
   */
  function setInput(method) {
    $('#edit-input').val($('#input-example-' + method).text());
    previousMethod = method;
  }
}
