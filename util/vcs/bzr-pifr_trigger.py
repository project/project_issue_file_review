# -*- coding: utf-8 -*-
# Provides a post commit plugin/hook for bazaar.
# Add this file to ~/.bazaar/plugins/
# @see http://doc.bazaar.canonical.com/development/en/user-reference/index.html#post-commit
# @see http://doc.bazaar.canonical.com/plugins/en/plugin-development.html#providing-custom-code-via-hooks
# @see http://schettino72.wordpress.com/category/testing/
# @see http://bazaar.launchpad.net/~bialix/%2Bjunk/checkeol/annotate/head%3A/__init__.py
# @see http://wiki.bazaar.canonical.com/BzrPlugins

from bzrlib import errors
from bzrlib.branch import Branch

def pre_commit_hook(local, master, old_revno, old_revid, future_revno, future_revid, tree_delta, future_tree):

    # Determine the user that made the commit.
    user = future_revid.split('-')[0]

    # Generate list of changed files to pass on to queue.php.
    file_list = []

    for path in tree_delta.added:
        file_list.append(path[0])

    for path in tree_delta.modified:
        file_list.append("" + path[0])

    for newpath in tree_delta.renamed:
        file_list.append("" + newpath[0])

    # Queue syntax/coder review of all touched files.
    queue_test(`future_revno`, user, ';'.join(file_list))

# Register pre_commit_hook with bazaar.
Branch.hooks.install_named_hook('pre_commit', pre_commit_hook, 'pifr_trigger')

def queue_test(revno, user, file_list):
    import httplib

    connection = httplib.HTTPConnection('dev.example.com')
    connection.request('GET', '/queue.php?revno=' + revno + '&user=' + user + '&file_list=' + file_list)

    if connection.getresponse().status != 200:
        raise errors.BzrError('unable to request queue.php.')

    connection.close()
