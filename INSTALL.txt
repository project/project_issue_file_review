
AUTHOR
------

  Jimmy Berry ("boombatower", http://drupal.org/user/214218)

REQUIREMENTS
------------

  * CLI
    - SimpleTest
      > curl
    - PIFR slave
      > cvs
      > curl
  * Drupal 7
    - Recent libjpeg
  * PHP
    - Configuration
      > Memory limit (CLI and APACHE configurations) recommended: 256MB
      > PHP short tag support must be disabled.
    - Extensions
      > php-cli
      > php-common
      > php-curl
      > php-gd
      > php-mbstring
      > php-(database extension)
      > php-pdo
      > php-xml
  * PIFR server
    - Chart API
      > http://drupal.org/project/chart
    - Views
      > http://drupal.org/project/views
    - Tabs
      > http://drupal.org/project/tabs
    - Clean URLs
      > http://drupal.org/node/15365
  * PIFR client
    - Must run from the webroot.
      > Example: example.com should point to Drupal installation with PIFR,
        then example.com/checkout should point to the testing code checkout.

INSTALLATION
------------

1.  Downloaded the latest PIFR release, install the code, and enable the PIFR
    module (admin/build/modules).

2.  Enabled PIFR Client and/or PIFR server depending on the role(s) the server
    will fulfill. Follow the additional setup instructions that apply to the
    roles selected.

PIFR SERVER:

1.  Visit admin/settings/chart and set the 'Global Background Color' to
    'FFFFFF00'.

PIFR CLIENT:

1.  Configure the client by filling in the appropriate values at
    admin/pifr/configuration.

2.  Create a symbolic link in the root of the Drupal installation to the site
    files directory and the "checkout" directory which should have been created
    when you enabled the client. The command to create the symbolic link, run
    from the Drupal root, should look like the following (where [site_folder]
    is replaced with the folder the client is running out of):

    ln -s ./sites/[site_folder]/files/checkout .

3.  Ensure that your server has the required PHP PDO exension, PHP version, and
    mysql version to support running Drupal 7. (Newer version of libjpeg, OS
    library, is required along with the PHP extension.)

4.  Set up a second database for the testing to occur in. The database
    information needs to be added to the settings.php with the key
    'pifr_checkout' file and should look something like the following.

    $db_url = array();
    $db_url['default'] = 'mysqli://user:password@localhost/drupal';
    $db_url['pifr_checkout'] = 'mysqli://user:password@localhost/drupal_checkout';

    Due to Drupal 7 requirements, the mysql user for the pifr_checkout database
    will also need permission to create temporary tables.
    (CREATE TEMPORARY TABLES)

    This second database is where the Drupal HEAD checkout will be installed
    and the actual testing will be performed. If possible, you should optimize
    the database for the rapid creation and deletion of tables.

    For SQLite the path is the only part used (see Drupal 7 for convention).
    Please ensure that the path points to a file inside the root of the
    checkout directory.

5.  You should set up a firewall to prevent malicious or buggy patches from causing
    issues on your server. Disabling the ability to send mail is also a good idea.

    The firewall rules should look something like the following:
      - Inbound:
        * HTTP - from testing.drupal.org.
      - Outbound:
        * HTTP - to testing.drupal.org and drupal.org.
        * CVS - to drupal.org HEAD CVS.

6.  Check the client configuration by visiting admin/pifr/configuration and
    correct any issues found.

7.  To test the client and determine what is wrong with it you can use the
    run tests locally tool (admin/pifr/run-test). It will attempt to run
    the client through a complete test run as it will do when reviewing.

8.  Once the client is ready to be added to the master server list, it is time
    to setup cron. Cron needs to run on the client every 5-10 minutes for
    optimal efficiency.

9.  Add the test client to PIFR Server 'server list'.
