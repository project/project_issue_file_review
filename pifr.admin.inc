<?php
/**
 * @file
 * Automatically review patches attached to issues.
 *
 * @author Jimmy Berry ("boombatower", http://drupal.org/user/214218)
 */

/**
 * General dashboard form.
 */
function pifr_admin_dashboard_form(&$form_state) {
  $form = array();

  // Server role.
  $form['role'] = array(
    '#type' => 'fieldset',
    '#title' => t('Role'),
    '#attributes' => array('class' => 'container-inline'),
    '#weight' => -10,
  );
  $form['role']['client'] = array(
    '#type' => 'item',
    '#title' => t('Client'),
    '#value' => theme('pifr_state', module_exists('pifr_client'), 'warning'),
  );
  $form['role']['server'] = array(
    '#type' => 'item',
    '#title' => t('Server'),
    '#value' => theme('pifr_state', module_exists('pifr_server'), 'warning'),
  );

  // Server status.
  $form['status'] = array(
    '#type' => 'fieldset',
    '#title' => t('Status'),
    '#attributes' => array('class' => 'container-inline'),
    '#weight' => -9,
  );
  $form['status']['state'] = array(
    '#type' => 'item',
    '#title' => t('State'),
    '#value' => theme('pifr_state', PIFR_ACTIVE),
  );
  $form['status']['toggle_state'] = array(
    '#type' => 'submit',
    '#value' => t('Toggle state'),
    '#submit' => array('pifr_admin_dashboard_form_toggle_submit'),
    '#weight' => 1000,
  );

  $form['cron'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cron'),
    '#description' => t(''),
    '#attributes' => array('class' => 'container-inline'),
    '#weight' => -8,
  );
  $form['cron']['last'] = array(
    '#type' => 'item',
    '#title' => t('Last run'),
    '#value' => t('!time ago', array('!time' => format_interval(time() - variable_get('cron_last', NULL)))),
  );
  $form['cron']['run'] = array(
    '#type' => 'markup',
    '#value' => l('(' . t('run cron') . ')', 'admin/reports/status/run-cron', array('query' => 'destination=admin/pifr')),
  );

  return $form;
}

/**
 * Toggle active variable.
 */
function pifr_admin_dashboard_form_toggle_submit($form, &$form_state) {
  variable_set('pifr_active', !PIFR_ACTIVE);
}

/**
 * Theme state value.
 *
 * @param boolean $enabled Enabled.
 * @param string $severity Severity of state if disabled: warning or error.
 * @return string HTML output.
 */
function theme_pifr_state($enabled, $severity = 'error') {
  drupal_add_css(drupal_get_path('module', 'pifr') . '/pifr.css');
  $word = $enabled ? 'enabled' : 'disabled';

  if ($severity == 'warning') {
    return t($word) . '<br />';
  }
  return '<span class="pifr-' . $word . '">' . t($word) . '</span><br />';
}

/**
 * General configuration form.
 */
function pifr_admin_configuration_form(&$form_state) {
  $form = array();

  $form['general'] = array(
    '#type' => 'fieldset',
    '#title' => t('General'),
    '#weight' => -10
  );
  $form['general']['pifr_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Debug'),
    '#description' => t('Control the output of detailed debug information to the watchdog log.'),
    '#default_value' => PIFR_DEBUG,
  );
  $form['general']['pifr_shortcut_core_test'] = array(
    '#type' => 'checkbox',
    '#title' => t('Shortcut Drupal core test'),
    '#description' => t('On a core test, causes only the NonDefaultBlockAdmin test to run, so a testbot can be confirmed easily in testing. <em>Ensure that this is disabled before finalizing a client.</em>'),
    '#default_value' => PIFR_SHORTCUT_CORE_TEST,
  );
  return system_settings_form($form);
}
